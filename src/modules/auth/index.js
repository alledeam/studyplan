import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { Login, ForgotPassword, ResetPassword } from "./pages";

function Auth({ match }) {
    return <Switch>
        <Route exact path={`${match.url}/forgot-password`} component={ForgotPassword} />
        <Route exact path={`${match.url}/reset-password`} component={ResetPassword} />
        <Route exact path={`${match.url}/login`} component={Login} />
        <Route exact path={""} render={()=><Redirect to={`${match.url}/login`}/>} />
    </Switch>
}

export default Auth;