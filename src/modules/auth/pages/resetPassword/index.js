import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import { Link, Redirect } from "react-router-dom";

import MyInput from "../../../shared/Input";
import Loader from "../../../shared/Loader";
import ShortLogo from "../../../../assets/images/primary/alledeam-logo-short.png";
import { doValidateCode, doResetNewPassword } from "../../redux/actions";



const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(14),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },

});

class ResetPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      email : '',
      code: '',
      form: {
        confirmPassword: "",
        password: ""
      },
      inValidInputs: {},
      isSubmitted: false,
      redirectToLogin: false,
      loading: true
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    const { inValidInputs } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0) {
      return;
    }
    this.props.doResetNewPassword({code :this.state.code, password :this.state.form.password})
    .then((d)=>{
        if(d){
            this.setState({ redirectToLogin : true })
        }
    })
  };
  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    const { form, inValidInputs } = this.state;
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    }else{
      delete inValidInputs[name];
    }
    this.setState({ form, inValidInputs });
  };
  componentDidMount(){
    const params = new URLSearchParams(this.props.location.search);
    // const [mode, oobCode, apiKey] = ["mode", "oobCode", "apiKey"].map(q=>params.get(q));
    const oobCode = params.get('oobCode');
    if(!oobCode){
        return this.setState({ redirectToLogin : true })
    }
    this.props.doValidateCode({code: oobCode})
    .then(email=>{
        if(email){
            this.setState({ email, loading : false, code: oobCode })
        }else{
            this.setState({ redirectToLogin : true })
        }
    })

  }
  render() {
    const { classes } = this.props;

    const { form, email, loading, redirectToLogin } = this.state;
    if(redirectToLogin){
        return <Redirect to="/auth" />
    }
    if(loading){
        return <Loader />
    }
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Paper className={classes.paper}>
          <img alt="logo"src={ShortLogo} className={"auth-logo"}/>
          <Typography component="h1" variant="h5">
            Reset Pasword
          </Typography>
          <Typography variant="body2">Hi {email}!. Go ahead and set your new password.</Typography>
          <form
            className={classes.form}
            noValidate
            onSubmit={this.handleSubmit}
          >
            <MyInput
              type="password"
              pattern=".{6,}"
              label={"New Password"}
              name={"password"}
              value={form.password}
              isSubmitted={this.state.isSubmitted}
              onChange={this.handleInputChange}
              errorMessage={"Password must have minimum 6 digits"}
              required
            />
            <MyInput
              type="password"
              label={"Confirm Password"}
              name={"confirmPassword"}
              value={form.confirmPassword}
              isSubmitted={this.state.isSubmitted}
              onChange={this.handleInputChange}
              errorMessage={"Passwords not matched"}
              checkValid={(val)=>form.password===val}
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Typography
                  variant="caption"
                  component={Link}
                  to="/auth"
                >
                 Back to sign in
                </Typography>
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Container>
    )
  }
}


const mapStateToProps = state => ({
  notification: state.notification
});

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps, { doValidateCode, doResetNewPassword })
)(ResetPassword);
