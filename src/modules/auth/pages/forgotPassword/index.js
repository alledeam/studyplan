import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";

import MyInput from "../../../shared/Input";
import ShortLogo from "../../../../assets/images/primary/alledeam-logo-short.png";
import { doResetPassword } from "../../redux/actions";

const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(14),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
});
class ForgotPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
      form: {
        email: "",
      },
      inValidInputs: {},
      isSubmitted: false
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    const { inValidInputs } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0) {
      return;
    }
    this.props.doResetPassword(this.state.form);
  };
  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    const { form, inValidInputs } = this.state;
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    } else {
      delete inValidInputs[name];
    }
    this.setState({ form, inValidInputs });
  };
  render() {
    const { classes } = this.props;
    const { form } = this.state;
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Paper className={classes.paper}>
        <img alt="logo" src={ShortLogo} className={"auth-logo"}/>
          <Typography component="h1" variant="h5">
            Recover password
          </Typography>
          <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
            <Typography variant="body2" className={"text-center"}>
              Enter your registered email address to recover your password.
            </Typography>
            <MyInput
              type="text"
              label={"Email"}
              name={"email"}
              value={form.email}
              isSubmitted={this.state.isSubmitted}
              onChange={this.handleInputChange}
              errorMessage={"Please enter a valid email"}
              pattern={
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
              }
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Submit
            </Button>
            <Grid container>
              <Grid item>
                <Typography variant="caption" component={Link} to="/auth">
                  Back to sign in
                </Typography>
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  notification: state.notification
});

export default compose(
  withStyles(useStyles),
  connect(
    mapStateToProps,
    { doResetPassword }
  )
)(ForgotPassword);
