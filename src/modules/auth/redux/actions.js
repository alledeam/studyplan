import firebase from "firebase/app";
import "firebase/auth";

import { showNotication } from "../../../redux/actions";

function handlePromise(fn, ...arg) {
  return (dispatch, successMsg = null) => {
    return fn(...arg)
      .then(data => {
        if (successMsg) {
          dispatch(
            showNotication({
              type: "success",
              message: successMsg
            })
          );
        }
        return data || {};
      })
      .catch(err => {
          console.log(err);
        dispatch(
          showNotication({
            type: "error",
            message: err.message
          })
        );
        return null;
      });
  };
}

export const doLogin = payload => dispatch => {
  const fbAuth = firebase.auth();
  const fn = firebase.auth().signInWithEmailAndPassword.bind(fbAuth);
  return handlePromise(fn, payload.email, payload.password)(
    dispatch,
    "User successfull logged in"
  );
};

export const doResetPassword = payload => dispatch => {
    const fbAuth = firebase.auth();
    const fn = firebase.auth().sendPasswordResetEmail.bind(fbAuth);
    return handlePromise(fn, payload.email)(
      dispatch,
      "A reset password mail is sent to your email."
    );
};

export const doValidateCode = payload => dispatch => {
    const fbAuth = firebase.auth();
    const fn = firebase.auth().verifyPasswordResetCode.bind(fbAuth);
    return handlePromise(fn, payload.code || "")(
      dispatch,
    );
};

export const doResetNewPassword = payload => dispatch => {
    const fbAuth = firebase.auth();
    const fn = firebase.auth().confirmPasswordReset.bind(fbAuth);
    return handlePromise(fn, payload.code || "", payload.password)(
      dispatch,
      "Password reset successfully. Login now."
    );
};

