import React from "react";
import { TextField, FormHelperText } from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Button from "@material-ui/core/Button";

class CommonInput extends React.Component {
  checkValid = value => {
    let errorMessage = "";
    let isValid = this.props.pattern
      ? value.match(new RegExp(this.props.pattern, "gi"))
      : true;
    if (!isValid) {
      errorMessage = this.props.errorMessage || `Invalid ${this.props.label}`;
    }
    if (this.props.required && !value) {
      isValid = false;
      errorMessage = `${this.props.label} is required`;
    }
    if (this.props.checkValid && !this.props.checkValid(value)) {
      isValid = false;
      errorMessage = this.props.errorMessage || `Invalid ${this.props.label}`;
    }
    return {
      isValid,
      errorMessage
    };
  };

  handleChange = e => {
    const value = (e || {}).target ? e.target.value : e || "";
    const { isValid } = this.checkValid(value);
    this.props.onChange({
      target: {
        name: this.props.name,
        value,
        isInvalid: !isValid
      }
    });
  };
  componentDidMount() {
    this.handleChange({
      target: {
        name: this.props.name,
        value: this.props.value
      }
    });
  }
  render() {
    const { isValid, errorMessage } = this.checkValid(this.props.value);
    const showError = this.props.isSubmitted && !isValid;
    let rest = {};
    if (this.props.multiline) {
      rest = {
        multiline: this.props.multiline,
        rowsMax: this.props.rowsMax,
        rows: this.props.multiline
      };
    }
    return (
      <div style={this.props.style}>
        {this.props.type === "date" ? (
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <div style={{ display: "flex" }}>
              <KeyboardDatePicker
                fullWidth
                style={{ flexGrow: 1 }}
                margin="normal"
                id="mui-pickers-date"
                label={this.props.label}
                value={this.props.value || ""}
                onChange={this.handleChange}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
              {this.props.showToday ? (
                <Button
                  variant="outlined"
                  color="primary"
                  style={{ alignSelf: "flex-end", margin: "0.5rem 1rem" }}
                  onClick={() =>
                    this.props.onChange({
                      target: {
                        name: this.props.name,
                        value: new Date(),
                        isInvalid: false
                      }
                    })
                  }
                >
                  Today
                </Button>
              ) : null}
            </div>
          </MuiPickersUtilsProvider>
        ) : this.props.type === "time" ? (
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardTimePicker
              fullWidth
              margin="normal"
              id="mui-pickers-time"
              label={this.props.label}
              value={this.props.value || ""}
              onChange={this.handleChange}
              KeyboardButtonProps={{
                "aria-label": "change time"
              }}
            />
          </MuiPickersUtilsProvider>
        ) : (
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            autoComplete="off"
            label={this.props.label}
            name={this.props.name}
            type={this.props.type}
            error={showError}
            value={this.props.value || ""}
            onChange={this.handleChange}
            style={{ ...(this.props.style || {}) }}
            {...rest}
          />
        )}

        {showError && (
          <FormHelperText className="error-text">{errorMessage}</FormHelperText>
        )}
      </div>
    );
  }
}

export default CommonInput;
