import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
  container: {
    width: "100%",
    display: "flex",
    justifyContent: "center"
  },
  progress: {
    margin: theme.spacing(6)
  }
}));

export default function CircularIndeterminate({ hide }) {
  const classes = useStyles();
  return hide ? (
    ""
  ) : (
    <div className={classes.container}>
      <CircularProgress className={classes.progress} />
    </div>
  );
}
