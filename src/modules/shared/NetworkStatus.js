import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import WifiOff from "@material-ui/icons/WifiOff";
import Close from "@material-ui/icons/Close";

const useStyles = makeStyles(theme => ({
  appBar: {
    top: "auto",
    bottom: 0,
    color: 'white',
    backgroundColor: "#ff5050"
  },
  grow: {
    flexGrow: 1
  }
}));

export default function NetworkStatus({ onClose }) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar style={{ justifyContent : 'space-between'}}>
          <div style={{ display : "flex" }}>
            <IconButton edge="start" color="inherit" aria-label="Open drawer">
              <WifiOff />
            </IconButton>
            <Typography component="h6" style={{ lineHeight : 3}}>No Internet Connection.</Typography>
          </div>

          <IconButton edge="start" color="inherit" aria-label="Open drawer" onClick={()=>onClose()}>
            <Close />
          </IconButton>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}
