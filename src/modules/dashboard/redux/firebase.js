import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import firebaseConfig from "../../../config/firebase.config";
import { deepCopy } from "../../../services/common";

export function getFirebaseAdminApp() {
  const alreadyExistApp = firebase.apps.find(f => f.name === "adminApp");
  if (alreadyExistApp) {
    return alreadyExistApp;
  } else {
    return firebase.initializeApp(firebaseConfig, "adminApp");
  }
}

export function getUsersList() {
  return firebase
    .firestore()
    .collection("users")
    .onSnapshot();
}

export function getUsersRole() {
  return firebase
    .firestore()
    .collection("users-role")
    .onSnapshot();
}

export function getDeactivatedUsersList() {
  return firebase
    .firestore()
    .collection("deactivated-users")
    .valueChanges();
}

export function getUserById(userId, cb) {
  return firebase
    .firestore()
    .collection("users")
    .doc(userId)
    .onSnapshot(doc => {
      cb(doc.data);
    });
}
export function updateUsername(username) {
  const time = new Date();
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("users")
    .doc(uid)
    .update({
      username,
      updatedAt: time
    });
}

export function updatePassword(password) {
  return firebase.auth().currentUser.updatePassword(password);
}

export function generatePassword(length) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function createUserAccount(form) {
  const details = form;
  details.password = generatePassword(12);
  const adminAuthApp = getFirebaseAdminApp();
  return adminAuthApp
    .auth()
    .createUserWithEmailAndPassword(details.email, details.password)
    .then(firebaseUser => {
      adminAuthApp.auth().signOut();
      return firebaseUser;
    })
    .then(response => {
      const time = new Date();
      const promise1 = response.user.updateProfile({
        displayName: details.username
      });
      const promise2 = firebase
        .firestore()
        .collection("/users")
        .doc(response.user.uid)
        .set({
          email: details.email,
          username: details.username,
          password: details.password,
          role: details.role,
          createdAt: time,
          updatedAt: time
        });
      return Promise.all([promise1, promise2]);
    });
}

export function updateUserAccountDetails({ uid, userDetails }) {
  return firebase
    .firestore()
    .collection("users")
    .doc(uid)
    .update(userDetails);
}

export function updateUserRole({ uid, role }) {
  return firebase
    .firestore()
    .collection("users-role")
    .doc(uid)
    .set({
      [role]: true
    });
}
export function activateORDeactivateAccount({ uid, activateUser }) {
  if (activateUser) {
    return firebase
      .firestore()
      .collection("deactivated-users")
      .doc(uid)
      .delete();
  }
  return firebase
    .firestore()
    .collection("deactivated-users")
    .doc(uid)
    .set({
      userId: uid,
      deactivatedAt: new Date()
    });
}

export function createStudyPlan({ title, description }) {
  const time = new Date();
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("study-plans")
    .add({
      title,
      description,
      createdAt: time,
      ownerId: uid,
      updatedAt: time,
      totalActivities: 0,
      totalBacklogActivities: 0,
      totalUsers: 0
    });
}

export function updateStudyPlan(planId, plan) {
  const time = new Date();
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .update({
      ...plan,
      updatedAt: time
    });
}

export function deleteStudyPlan(planId) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .delete();
}

export function snapshotsToData(snaps) {
  const data = [];
  snaps.forEach(doc => {
    data.push({ ...doc.data(), docId: doc.id });
  });
  return data;
}

export function getPlans(cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .orderBy("title", "asc")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

export function getUsers(cb) {
  return firebase
    .firestore()
    .collection("users")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

export function saveUsers({ users, plan }) {
  const userPromise = users.map(user => {
    return firebase
      .firestore()
      .collection("study-plans")
      .doc(plan.docId)
      .collection("users")
      .doc(user.value)
      .set({
        addedAt: new Date()
      });
  });
  return Promise.all(userPromise);
}

export function deleteStudyPlanUsers({ user, plan }) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(plan.docId)
    .collection("users")
    .doc(user.docId)
    .delete();
}
export function getPlanById(planId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .onSnapshot(doc => {
      cb(doc.data());
    });
}

export function getActivityById(planId, activityId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId)
    .onSnapshot(doc => {
      cb(doc.data());
    });
}

export function getActivities(planId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}
export function createPlanActivity(planId, activity) {
  const time = new Date();
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .add({
      ...activity,
      updatedAt: time,
      createdAt: time,
      totalComments: 0,
      startTime: time,
      isCompleted: false,
      ownerId: uid
    });
}

export function updateActivityStatus(planId, activityId, isCompleted) {
  const uid = firebase.auth().currentUser.uid;
  const meta = {};
  if (isCompleted) {
    meta.completedBy = uid;
    meta.completedAt = new Date();
    meta.isPinned = false;
  } else {
    meta.openedBy = uid;
    meta.openedAt = new Date();
  }
  return updateActivity(planId, activityId, {
    ...meta,
    isCompleted
  });
}

export function updateActivityPinStatus(planId, activityId, isPinned) {
  return updateActivity(planId, activityId, {
    isPinned
  });
}
export function updateActivity(planId, activityId, activity) {
  const time = new Date();
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId)
    .update({
      ...activity,
      updatedAt: time
    })
    .then(d => {
      return d || {};
    });
}

export function deleteActivity(planId, activityId) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId)
    .delete();
}

export function getActivitiesOrder(planId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities-order")
    .doc("order")
    .onSnapshot(doc => {
      cb(doc.data());
    });
}

export function setActivitiesOrder(planId, activitiesOrder) {
  const time = new Date();
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities-order")
    .doc("order")
    .set({ order: activitiesOrder, updatedBy: uid, updatedAt: time });
}

export function getStudyPlanUsers(planId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("users")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

export function getMyPlans(cb) {
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("users-study-plans")
    .doc(uid)
    .onSnapshot(doc => {
      cb(doc.data());
    });
}

export function addCommnet(planId, activityId, comment) {
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId)
    .collection("comments")
    .add({
      userId: uid,
      createdAt: new Date(),
      ...comment
    });
}

export function fetchCommnet(planId, activityId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId)
    .collection("comments")
    .orderBy("createdAt", "asc")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

export function addTickets(userId, ticket) {
  const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("users-tickets")
    .doc(userId)
    .set(
      {
        tickets: firebase.firestore.FieldValue.arrayUnion({
          ...ticket,
          createdAt: new Date(),
          createdBy: uid
        })
      },
      { merge: true }
    );
}

export function setActive(userId, ticket, active) {
  const { type, ...removeTicket } = ticket;
  return firebase
    .firestore()
    .collection("users-tickets")
    .doc(userId)
    .set(
      {
        tickets: firebase.firestore.FieldValue.arrayRemove(removeTicket)
      },
      { merge: true }
    )
    .then(() => {
      console.log("reached here");
      return firebase
        .firestore()
        .collection("users-tickets")
        .doc(userId)
        .set(
          {
            tickets: firebase.firestore.FieldValue.arrayUnion({
              ...removeTicket,
              active
            })
          },
          { merge: true }
        );
    });
}

// export function wTickets(users){
//   users.map(user=>{
//     return firebase
//       .firestore()
//       .collection("users-tickets")
//       .doc(user.docId)
//       .update({
//         role: 'STUDENT',
//         registered: [],
//         usages : []
//       })
//   })
// }
export function updateRole(uid, role) {
  const time = new Date();
  return firebase
    .firestore()
    .collection("users")
    .doc(uid)
    .update({
      role,
      updatedAt: time
    });
}
export function getUsersTickets(cb) {
  return firebase
    .firestore()
    .collection("users-tickets")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

export function getMyTickets(cb) {
  const uid = ((firebase.auth() || {}).currentUser || {}).uid;
  if (uid) {
    return firebase
      .firestore()
      .collection("users-tickets")
      .doc(uid)
      .onSnapshot(doc => {
        cb(doc.data());
      });
  }
}
export function getUserPlan(userId, cb) {
  return firebase
    .firestore()
    .collection("users-study-plans")
    .doc(userId)
    .onSnapshot(doc => {
      cb(doc.data());
    });
}
export function getUsersPlan(userId) {
  return firebase
    .firestore()
    .collection("users-study-plans")
    .doc(userId)
    .get()
    .then(doc => {
      return doc.data();
    });
}
export function getPlanActivities(planId) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .get()
    .then(docs => {
      const array = [];
      docs.forEach(doc => {
        array.push({
          docId: doc.id,
          ...doc.data()
        });
      });
      return array;
    });
}
export function getPlansUser(planId) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("users")
    .get()
    .then(docs => {
      const array = [];
      docs.forEach(doc => {
        array.push({
          docId: doc.id,
          ...doc.data()
        });
      });
      return array;
    });
}
export function getPlanFromPlanId(planId) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .get()
    .then(doc => {
      return { data: doc.data(), id: doc.id };
    });
}

export function getLoginId() {
  return ((firebase.auth() || {}).currentUser || {}).uid;
}
//need to be deleted
export function getPlanUsers(planId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("users")
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

export function getRole(users) {
  if (Object.prototype.toString.call(users) === "[object Array]") {
    const userPromise = users.map(user => {
      return firebase
        .firestore()
        .collection("users")
        .doc(user.docId)
        .get()
        .then(doc => {
          return doc.data();
        });
    });
    return Promise.all(userPromise);
  } else {
    return firebase
      .firestore()
      .collection("users")
      .doc(users)
      .get()
      .then(doc => {
        return doc.data();
      });
  }
}
export function getUserTickets(users) {
  let userPromise = users.map((user, i) => {
    return firebase
      .firestore()
      .collection("users-tickets")
      .doc(user.docId)
      .get()
      .then(doc => {
        return doc.data();
      });
  });
  return Promise.all(userPromise);
}
export function updateUserTicket({ userId, oldTicket, newTicket }) {
  const sfDocRef = firebase
    .firestore()
    .collection("users-tickets")
    .doc(userId);
  return firebase.firestore().runTransaction(function(transaction) {
    return transaction.get(sfDocRef).then(function(sfDoc) {
      const newValue = sfDoc.data();
      let tickets = newValue.tickets;
      let updated = tickets.map(t => {
        let ticket = deepCopy(t);
        if (t.createdAt.isEqual(oldTicket.createdAt)) {
          ticket.updatedAt = new Date();
          ticket.note = newTicket.note;
          ticket.minutes = newTicket.minutes;
          ticket.date = newTicket.date;
          return ticket;
        }
        return t;
      });
      return transaction.update(sfDocRef, {
        tickets: updated
      });
    });
  });
}
export function deleteUserTicket({ userId, ticket }) {
  debugger;
  const sfDocRef = firebase
    .firestore()
    .collection("users-tickets")
    .doc(userId);
  return firebase.firestore().runTransaction(function(transaction) {
    return transaction.get(sfDocRef).then(function(sfDoc) {
      const newValue = sfDoc.data();
      let tickets = newValue.tickets;
      let updated = tickets.filter(t => {
        if (!t.createdAt.isEqual(ticket.createdAt)) {
          return t;
        }
      });
      return transaction.update(sfDocRef, {
        tickets: updated
      });
    });
  });
}

export function getUserTicket(uid, cb) {
  // const uid = firebase.auth().currentUser.uid;
  return firebase
    .firestore()
    .collection("users-tickets")
    .doc(uid)
    .onSnapshot(doc => {
      cb({ docId: doc.id, ...doc.data() });
    });
}

export function getActivityData(planId, activityId, cb) {
  return firebase
    .firestore()
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId)
    .onSnapshot(snaps => {
      cb(snapshotsToData(snaps));
    });
}

// delete User Account
export function deleteUserAccount(user) {
  const uid = firebase.auth().currentUser.uid;
  const deletedAt = new Date();
  return Promise.all([
    firebase
      .firestore()
      .collection("deleted-users")
      .doc(user.docId || user.uid)
      .set({
        email: user.email || "",
        username: user.username || "",
        userId: user.docId || user.uid,
        deletedBy: uid,
        role: user.role || "",
        deletedAt
      }),
    firebase
      .firestore()
      .collection("users")
      .doc(user.docId || user.uid)
      .update({
        isDeleted: true,
        deletedAt
      })
  ]);
}
