import { formatDistance } from "date-fns";

function hashCode(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}

export function getUniqueColorFromID(userID){
    return "#" + intToRGB(hashCode(userID))
}

export function timeAgo(fsTime){
    return formatDistance(new Date(), new Date(fsTime.toDate()), {
        addSuffix: true,
        addPrefix: false,
        includeSeconds: true
      });
}
export function urlify(text) {
    const urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return `<a href="${url}" target="__blank">${url}</a>`;
    })

}
