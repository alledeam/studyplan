import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

import { showNotication, setDraggable } from "../../../redux/actions";
import {
  createUserAccount,
  createStudyPlan,
  updateStudyPlan,
  deleteStudyPlan,
  createPlanActivity,
  saveUsers,
  updatePassword,
  updateUsername,
  addTickets,
  updateRole,
  updateUserTicket,
  deleteUserTicket,
  deleteUserAccount,
  setActive
} from "./firebase";

function handlePromise(fn, ...arg) {
  return (dispatch, successMsg = null) => {
    return fn(...arg)
      .then(data => {
        if (successMsg) {
          dispatch(
            showNotication({
              type: "success",
              message: successMsg
            })
          );
        }
        return data || {};
      })
      .catch(err => {
        dispatch(
          showNotication({
            type: "error",
            message: err.message
          })
        );
        return null;
      });
  };
}

export const doLogout = () => dispatch => {
  const fbAuth = firebase.auth();
  const fn = fbAuth.signOut.bind(fbAuth);
  return handlePromise(fn)(dispatch, "User successfull logged out");
};

export const doRegister = payload => dispatch => {
  return handlePromise(createUserAccount, payload)(
    dispatch,
    "User successfull created"
  );
};

export const doUpdatePassword = password => dispatch => {
  return handlePromise(updatePassword, password)(
    dispatch,
    "Password successfull updated"
  );
};

export const doUpdateUsername = username => dispatch => {
  return handlePromise(updateUsername, username)(
    dispatch,
    "Username successfull updated"
  );
};

export const createPlan = payload => dispatch => {
  return handlePromise(createStudyPlan, payload)(
    dispatch,
    "Study plan successfull created"
  );
};

export const updatePlan = (planId, plan) => dispatch => {
  return handlePromise(
    updateStudyPlan,
    planId,
    plan
  )(dispatch, "Study plan successfull updated");
};

export const deletePlan = planId => dispatch => {
  return handlePromise(deleteStudyPlan, planId)(
    dispatch,
    "Study plan successfull deleted"
  );
};

export const addUsers = payload => dispatch => {
  return handlePromise(saveUsers, payload)(dispatch, "Users successfull added");
};

export const createActivity = payload => dispatch => {
  return handlePromise(
    createPlanActivity,
    payload.planId,
    payload.activity
  )(dispatch, "Activity successfull created");
};

export const createTicket = payload => dispatch => {
  return handlePromise(
    addTickets,
    payload.userId,
    payload.ticket
  )(dispatch, "Ticket added successfully.");
};

export const setTicketActive = payload => dispatch => {
  return handlePromise(
    setActive,
    payload.userId,
    payload.ticket,
    payload.active
  )(dispatch, "State of ticket updated successfully.");
};

export const updateUserRole = ({ user, role }) => dispatch => {
  return handlePromise(
    updateRole,
    user.uid,
    role
  )(dispatch, "Role updated successfully");
};
export const finishTimeError = () => dispatch => {
  return dispatch(
    showNotication({
      type: "error",
      message: "Please enter finish time first."
    })
  );
};

export const setDraggableEle = payload => dispatch => {
  return dispatch(setDraggable(payload));
};

export const updateTicketForUser = ({
  userId,
  oldTicket,
  newTicket
}) => dispatch => {
  return handlePromise(updateUserTicket, { userId, oldTicket, newTicket })(
    dispatch,
    "Ticket updated successfully"
  );
};

export const deleteTicketForUser = ({ userId, ticket }) => dispatch => {
  return handlePromise(deleteUserTicket, { userId, ticket })(
    dispatch,
    "Ticket deleted successfully"
  );
};

export const deleteUser = user => dispatch => {
  return handlePromise(deleteUserAccount, user)(
    dispatch,
    "User deleted successfully"
  );
};
