import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import ClearIcon from '@material-ui/icons/Clear';
import history from "../../../history";

const useStyles = makeStyles(theme => ({
 root:{
     position :"absolute",
     top : "74px",
     right: "10px"
 },
  fab: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

export default function GoBack() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Fab aria-label="Close" size="small" color="secondary" className={classes.fab} onClick={()=>history.goBack()}>
        <ClearIcon />
      </Fab>
    </div>
  );
}
