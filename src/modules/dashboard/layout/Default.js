import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import Header from "./Header";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1, 2),
    marginLeft: "-8px"
  },
  grow: {
    flexGrow: 1
  },
  container: {
    padding: 20,
    maxWidth: "1000px",
    margin: "auto"
  },
  main: {
    marginTop: 10
  },
  link: {
    display: "flex"
  },
  icon: {
    marginRight: theme.spacing(0.5),
    width: 20,
    height: 20
  }
}));

function DefaultLayout(props) {
  const params = props.location.pathname.split("/").filter(p=>p);
  const planId = params[2];
  const activityId = params[3];
  const classes = useStyles();
  return (
    <div className={classes.grow}>
      <Header {...props} />
      <div className={classes.container}>
        <Grid container>
          <Grid item>
            <Paper elevation={0} className={classes.root}>
              <Breadcrumbs aria-label="Breadcrumb" style={{fontStyle: "italic" }}>
                <Link to={`/dashboard/plans}`}>Dashboard</Link>
                {
                  planId && <Link to={`/dashboard/plans/${planId}`}>Study Plan</Link>
                }
                {
                  activityId && <Link to={`/dashboard/plans/${planId}/${activityId}`}>Activity</Link>
                }
              </Breadcrumbs>
            </Paper>
          </Grid>
        </Grid>
        <Grid
          container
          className={classes.main}
          spacing={2}
          alignContent="space-around"
        >
          {props.children}
        </Grid>
      </div>
    </div>
  );
}

export default DefaultLayout;
