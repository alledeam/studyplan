import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Home, StudyPlan, Activity, Users, SetPassword, UpdateProfle,UserTicketHistory } from "./pages";
import Layout from "./layout/Default";

import { doLogout } from "./redux/actions";

function MyLayout(props) {
  const { children, isAdmin, isLoggedIn, userDetails, onlyAdmin } = props;
  const Comp = <Layout
        {...props}
        isAdmin={isAdmin}
        userDetails={userDetails}
        logout={props.doLogout}
      >
        {children}
    </Layout>
  return isLoggedIn ? (
    onlyAdmin
    ? isAdmin
      ? Comp : <Redirect to="/dashboard" />
    : Comp
  ) : (
    <Redirect to="/auth" />
  );
}
function TeacherLayout(props) {
  const { children, isAdmin, isLoggedIn, userDetails } = props;
  let onlyTeacher = !props.userDetails.role
    ? false
    : props.userDetails.role === "STUDENT"||"TEACHER"
    ? true
    : false;
  const Comp = (
    <Layout
      {...props}
      isAdmin={isAdmin}
      userDetails={userDetails}
      logout={props.doLogout}
    >
      {children}
    </Layout>
  );
  return isLoggedIn ? (
    onlyTeacher ? (
      Comp
    ) : (
      <Redirect to="/dashboard" />
    )
  ) : (
    <Redirect to="/auth" />
  );
}
function Dashboard(props) {
  const match = props.match;
  return (
    <Switch>
       <Route
        exact
        path={`${match.url}/ticket-history`}
        render={() => (
          <TeacherLayout {...props} onlyAdmin>
            <UserTicketHistory {...props} />
          </TeacherLayout>
        )}
      />
      <Route
        exact
        path={`${match.url}/plans/:planId/:activityId`}
        render={() => (
          <MyLayout {...props} goBack>
            <Activity {...props} />
          </MyLayout>
        )}
      />
      <Route
        exact
        path={`${match.url}/plans/:planId`}
        render={() => (
          <MyLayout {...props} goBack>
            <StudyPlan {...props} />
          </MyLayout>
        )}
      />
      <Route
        exact
        path={`${match.url}/plans`}
        render={() => (
          <MyLayout {...props}>
            <Home {...props} />
          </MyLayout>
        )}
      />
      <Route
        exact
        path={`${match.url}/setPassword`}
        render={() => (
            <SetPassword {...props} />
        )}
      />
      <Route
        exact
        path={`${match.url}/username`}
        render={() => (
            <UpdateProfle {...props} />
        )}
      />
      <Route
        exact
        path={`${match.url}/users`}
        render={() => (
          <MyLayout {...props} onlyAdmin>
            <Users {...props} />
          </MyLayout>
        )}
      />
      <Route
        exact
        path={""}
        render={() => <Redirect to={`${match.url}/plans`} />}
      />
    </Switch>
  );
}

const mapStateToProps = state => ({
  isLoggedIn: state.isLoggedIn,
  isAdmin: state.isAdmin,
  userDetails: state.userDetails
});

export default connect(
  mapStateToProps,
  { doLogout }
)(Dashboard);
