import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { connect } from "react-redux";
import { getMyTickets, getLoginId } from "../../redux/firebase";
import PurchaseHistory from "../users/components/PuchaseHostory";
import UserActivities from "../users/components/userActivities";
import StudentActivities from "../users/components/studentActivities";
const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(14),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  root: {
    padding: theme.spacing(1, 2),
    width: "100%"
  },
  table: {
    width: "100%"
  }
}));

function TicketHistory(props) {
  const [formData, setFormData] = React.useState({
    tickets: []
  });
  React.useEffect(() => {
    const unSubscribeFn = getMyTickets(ticket => {
      setFormData({ ...formData, tickets: (ticket || {}).tickets });
      return function() {
        unSubscribeFn();
      };
    });
  }, [formData]);
  const classes = useStyles();
  return (
    <>
      {props.userDetails.role === "STUDENT" && (
        <Paper className={classes.root}>
          {formData.tickets && formData.tickets.length ? (
            <>
              <PurchaseHistory stats={formData} />
            </>
          ) : (
            <Typography variant="body2">No tickets to show!</Typography>
          )}
          {getLoginId() && (
            <StudentActivities
              user={{ uid: getLoginId() }}
              role={props.userDetails.role}
              users={props.users}
            />
          )}
        </Paper>
      )}
      {props.userDetails.role === "TEACHER" && (
        <Paper className={classes.root}>
          <>
            {getLoginId() && (
              <UserActivities
                user={{ uid: getLoginId() }}
                role={props.userDetails.role}
                users={props.users}
              />
            )}
          </>
        </Paper>
      )}
    </>
  );
}

const mapStateToProps = state => ({
  users: state.users,
  notification: state.notification,
  userDetails: state.userDetails
});

export default connect(mapStateToProps, {})(TicketHistory);
