import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import { Link, Redirect } from "react-router-dom";

import MyInput from "../../../shared/Input";
import { doUpdatePassword } from "../../redux/actions";

const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(14),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
});

class SetPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      done: false,
      form: {
        password: ""
      },
      inValidInputs: {},
      isSubmitted: false,
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    const { inValidInputs } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0) {
      return;
    }
    this.props.doUpdatePassword(this.state.form.password)
    .then(()=>{
      this.setState({done: true })
    })
  };
  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    const { form, inValidInputs } = this.state;
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    }else{
      delete inValidInputs[name];
    }
    this.setState({ form, inValidInputs });
  };
  render() {
    const { classes, userDetails } = this.props;
    const { done, form } = this.state;
    return !done ? (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Paper className={classes.paper}>
          <Typography
            className={classes.title}
            variant="h2"
            noWrap
            color="primary"
          >
            <span className="appTitle">@ Alledeam</span>
          </Typography>
          <Typography component="h1" variant="h5">
            Set New Password
          </Typography>
          <Typography variant="body2">Hi! {userDetails.username}. Go ahead and set your new password.</Typography>
          <form
            className={classes.form}
            noValidate
            onSubmit={this.handleSubmit}
          >
            <MyInput
              type="password"
              pattern=".{6,}" 
              label={"New Password"}
              name={"password"}
              value={form.password}
              isSubmitted={this.state.isSubmitted}
              onChange={this.handleInputChange}
              errorMessage={"Password must have minimum 6 digits"}
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Update Password
            </Button>
            <Grid container>
              <Grid item xs>
                <Typography
                  variant="caption"
                  component={Link}
                  to="/dashboard"
                >
                  Back to Home
                </Typography>
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Container>
    ) : (
      <Redirect to="/dashboard" />
    );
  }
}


const mapStateToProps = state => ({
  notification: state.notification
});

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps, { doUpdatePassword })
)(SetPassword);
