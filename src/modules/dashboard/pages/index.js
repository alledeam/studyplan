export { default as Home } from "./home"
export { default as StudyPlan } from "./study-plans";
export { default as Activity } from "./activity";
export { default as SetPassword } from "./setPassword";
export { default as UpdateProfle } from "./updateProfle";
export { default as Users } from "./users";
export { default as UserTicketHistory } from "./user-ticket-history";
