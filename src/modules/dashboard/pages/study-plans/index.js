import React from 'react';
import { Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { reorder } from 'react-reorder';

import Activities from './components/Activities';
import Plan from '../../common/Plan';
import Loader from '../../../shared/Loader';
import {
  getPlanById,
  getActivities,
  setActivitiesOrder,
  getActivitiesOrder
} from '../../redux/firebase';
import { createActivity } from '../../redux/actions';
import CreatePlan from '../home/components/CreatePlan';
import { updatePlan, deletePlan } from '../../redux/actions';

const useStyles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: '100%'
  }
});

class StudyPlan extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
      planId: '',
      plan: null,
      planLoading: true,
      activityLoading: true,
      inValidPlanId: false,
      activities: [],
      pinnedActivities: [],
      backLogsActivities: [],
      completedActivities: []
    };
  }
  listerers = [];
  fetchPlan = planId => {
    const unSubscribeFn = getPlanById(planId, plan => {
      if (plan) {
        this.setState({ plan: { ...plan, docId: planId }, planLoading: false });
      } else {
        this.setState({ inValidPlanId: true, planLoading: false });
      }
    });
    this.listerers.push(unSubscribeFn);
  };
  fetchActivityOrder = planId => {
    const unSubscribeFn = getActivitiesOrder(planId, data => {
      if (data) {
        this.setState(
          () => ({ activitiesOrder: data.order, orderLoading: false }),
          () => {
            this.rearrangeActivities();
          }
        );
      } else {
        this.setState({ activitiesOrder: {}, orderLoading: false });
      }
    });
    this.listerers.push(unSubscribeFn);
  };
  fetchActivities = planId => {
    const unSubscribeFn = getActivities(planId, data => {
      if (data) {
        const completedActivities = data
          .filter(a => a.isCompleted)
          .sort((a, b) => {
            return (
              (b.finishTime ? b.finishTime.toDate() : b.completedAt.toDate()) -
              (a.finishTime ? a.finishTime.toDate() : a.completedAt.toDate())
            );
          });
        const other = data.filter(a => !a.isCompleted);
        const backLogsActivities = other.filter(a => a.isBacklog);
        const liveActivities = other.filter(a => !a.isBacklog);
        let activities = liveActivities;
        const pinnedActivities = activities.filter(a => a.isPinned);
        activities = activities.filter(a => !a.isPinned);
        this.setState(
          () => ({
            backLogsActivities,
            activities,
            completedActivities,
            pinnedActivities,
            activityLoading: false
          }),
          () => {
            this.rearrangeActivities();
          }
        );
      }
    });
    this.listerers.push(unSubscribeFn);
  };
  rearrangeActivities = () => {
    const { activities, activitiesOrder } = this.state;
    if ((activitiesOrder || []).length > 0 && (activities || []).length > 0) {
      this.setState(() => {
        const orderedActivities = activitiesOrder
          .map(activityId => activities.find(a => a.docId === activityId))
          .filter(d => d);
        const activitiesWithoutOrder = activities.filter(
          act => !activitiesOrder.find(id => id === act.docId)
        );
        return {
          activities: [...activitiesWithoutOrder, ...orderedActivities]
        };
      });
    }
  };
  componentDidMount() {
    const params = this.props.location.pathname.split('/').filter(p => p);
    const planId = params[2];
    this.setState({ planId });
    this.fetchPlan(planId);
    this.fetchActivityOrder(planId);
    this.fetchActivities(planId);
  }
  componentWillUnmount() {
    this.listerers.map(unSubscribeFn => unSubscribeFn());
  }

  onDragEnd = (fromIndex, toIndex) => {
    const { activities } = this.state;
    const item = activities.splice(fromIndex, 1)[0];
    activities.splice(toIndex, 0, item);
    this.setState({ activities });
    setActivitiesOrder(this.state.planId, activities.map(act => act.docId));
  };
  onReorder = (event, previousIndex, nextIndex, fromId, toId) => {
    const activities = reorder(this.state.activities, previousIndex, nextIndex);
    this.setState({
      activities
    });
    setActivitiesOrder(this.state.planId, activities.map(act => act.docId));
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  handleEdit = () => {
    this.setState({ open: true });
  };
  deleteP = () => {
    this.props.deletePlan(this.state.planId).then(() => {
      this.setState({ inValidPlanId: true });
    });
  };
  render() {
    const {
      classes,
      createActivity,
      isAdmin,
      draggable,
      updatePlan
    } = this.props;
    const {
      open,
      plan,
      planId,
      planLoading,
      activityLoading,
      orderLoading,
      inValidPlanId,
      backLogsActivities,
      activities,
      completedActivities,
      pinnedActivities
    } = this.state;

    if (inValidPlanId) {
      return <Redirect to='/dashboard/plans' />;
    }

    if (planLoading || orderLoading || !plan) {
      return <Loader />;
    }
    return (
      <div className={classes.root}>
        <Plan
          completedActivities={completedActivities}
          plan={plan}
          isAdmin={isAdmin}
          handleSelectedPlan={this.handleEdit}
          deletePlan={() => this.deleteP()}
          noLink
        />
        {activityLoading ? (
          <Loader />
        ) : (
          <Activities
            isAdmin={isAdmin}
            planId={planId}
            loading={activityLoading}
            activities={activities}
            pinnedActivities={pinnedActivities}
            completedActivities={completedActivities}
            backLogsActivities={backLogsActivities}
            onDragEnd={this.onDragEnd}
            onReorder={this.onReorder}
            draggable={draggable}
            createActivity={activity =>
              createActivity({ planId: planId, activity })
            }
          />
        )}
        {open && (
          <CreatePlan
            open={open}
            handleClose={this.handleClose}
            isAdmin={isAdmin}
            updatePlan={updatePlan}
            plan={plan}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  draggable: state.draggable
});

export default compose(
  withStyles(useStyles),
  connect(
    mapStateToProps,
    { createActivity, updatePlan, deletePlan }
  )
)(StudyPlan);
