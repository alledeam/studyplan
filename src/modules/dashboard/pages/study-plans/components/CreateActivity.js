import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

const styles = (theme) => ({
  buttonContainer: {
    width: "100%",
    textAlign: "right",
  },
});

class CreateActivity extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activity: props.activity || "",
    };
  }
  handleSubmit = () => {
    const { activity } = this.state;
    if (!activity) {
      return;
    }
    this.props.createActivity({ activity });
    this.setState({ activity: "" });
  };
  handleInputChange = ({ target: { value } }) => {
    this.setState({ activity: value });
  };

  render() {
    const { classes, edit } = this.props;
    const { activity } = this.state;
    return (
      <>
        <TextField
          onChange={this.handleInputChange}
          value={activity}
          id="outlined-dense-multiline"
          label={edit ? "Update Activity" : "New Activity"}
          margin="dense"
          variant="outlined"
          multiline
          style={{ width: "100%" }}
          rowsMax="18"
          rows="4"
        />
        <div className={classes.buttonContainer}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            disabled={activity.length === 0}
            onClick={this.handleSubmit}
          >
            {edit ? "Update" : "Create"}
          </Button>
        </div>
      </>
    );
  }
}
export default withStyles(styles)(CreateActivity);
