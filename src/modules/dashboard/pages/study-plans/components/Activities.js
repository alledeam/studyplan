import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import Reorder from 'react-reorder';
import CreateActivity from "./CreateActivity";
import Empty from "../../../common/Empty";
import Activity from "../../activity";

const styles = theme => ({
  activity: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    marginTop: "20px",
    borderRadius: "5px",
    // "&:hover": {
    //   transform: "scale(1.01)",
    //   boxShadow:
    //     "0 15px 24px rgba(0, 0, 0, 0.22), 0 19px 76px rgba(0, 0, 0, 0.3)"
    // }
  },
  pinnedActivity: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    border: "solid 2px #ae0e0f",
    borderRadius: "5px",
    marginTop: "20px",
    "&:hover": {
      transform: "scale(1.01)",
      boxShadow:
        "0 15px 24px rgba(0, 0, 0, 0.22), 0 19px 76px rgba(0, 0, 0, 0.3)"
    }
  },
  inline: {
    display: "inline"
  },
  container: {
    padding: "20px",
    background: "linear-gradient(#ae0e0f2e,#ae0e0fad)",
    borderRadius: "0px 0px 5px 5px",
    minHeight: "500px"
  },
  dragIcon: {
    cursor: "move",
    position: "absolute",
    right: "10px",
    top: "8px",
    zIndex: 999
  },
  tab: {
    width: "33.33%",
    maxWidth: "33.33%",
    backgroundColor: "white",
    color: "#ae0f0f",
    opacity: 1,
    fontWeight: 700,
    border: "0.5px solid #ae0f0f",
    borderTop: "0px",
    borderBottom: "0px"
  },
  activeTab: {
    width: "33.33%",
    maxWidth: "33.33%",
    backgroundColor: "inherit",
    color: "inherit",
    opacity: 1,
    fontWeight: 700,
    border: "0.5px solid #ae0f0f",
    borderTop: "0px",
    borderBottom: "0px"
  },
  input: {
    marginBottom: "20px",
    padding: "10px"
  }
});

class Activities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0
    };
  }

  handleChange = (event, newValue) => {
    this.setState({ selectedTab: newValue });
  };

  render() {
    const {
      classes,
      createActivity,
      activities,
      pinnedActivities,
      backLogsActivities,
      completedActivities,
      planId,
      draggable,
      isAdmin
    } = this.props;

    const { selectedTab } = this.state;
    return (
      <>
        <AppBar position="static">
          <Tabs value={selectedTab} onChange={this.handleChange}>
            <Tab
              label="Open Activities"
              className={selectedTab === 0 ? classes.activeTab : classes.tab}
            />
            <Tab
              label="Completed Activities"
              className={selectedTab === 1 ? classes.activeTab : classes.tab}
            />
            <Tab
              label="Current Needs"
              className={selectedTab === 2 ? classes.activeTab : classes.tab}
            />
          </Tabs>
        </AppBar>
        {selectedTab === 0 && (
          <div className={classes.container}>
            <Paper className={classes.input}>
              <CreateActivity
                createActivity={activity =>
                  createActivity({ ...activity, isBacklog: selectedTab === 2 })
                }
              />
            </Paper>
            <Empty
              hide={pinnedActivities.length > 0 || activities.length > 0}
              text={"No activity yet!"}
            />
            {pinnedActivities.map(activity => (
              <div
                className={classes.pinnedActivity}
                key={activity.docId}
              >
                <Activity
                  isAdmin={isAdmin}
                  planId={planId}
                  activityId={activity.docId}
                  activity={activity}
                  cardView
                />
              </div>
            ))}
              <Reorder
                reorderId="my-list" // Unique ID that is used internally to track this list (required)
                reorderGroup="reorder-group" // A group ID that allows items to be dragged between lists of the same group (optional)
                // getRef={this.storeRef.bind(this)} // Function that is passed a reference to the root node when mounted (optional)
                component="div" // Tag name or Component to be used for the wrapping element (optional), defaults to 'div'
                placeholderClassName="placeholder" // Class name to be applied to placeholder elements (optional), defaults to 'placeholder'
                draggedClassName="dragged" // Class name to be applied to dragged elements (optional), defaults to 'dragged'
                lock="horizontal" // Lock the dragging direction (optional): vertical, horizontal (do not use with groups)
                // holdTime={50} // Default hold time before dragging begins (mouse & touch) (optional), defaults to 0
                touchHoldTime={500} // Hold time before dragging begins on touch devices (optional), defaults to holdTime
                mouseHoldTime={300} // Hold time before dragging begins with mouse (optional), defaults to holdTime
                onReorder={this.props.onReorder} // Callback when an item is dropped (you will need this to update your state)
                autoScroll={true} // Enable auto-scrolling when the pointer is close to the edge of the Reorder component (optional), defaults to true
                disabled={!draggable} // Disable reordering (optional), defaults to false
                disableContextMenus={true} // Disable context menus when holding on touch devices (optional), defaults to true
                // placeholder={
                //   <div className="custom-placeholder" /> // Custom placeholder element (optional), defaults to clone of dragged element
                // }
              >
                {
                  activities.map(activity => (
                    <div
                      className={classes.activity + (draggable ? " drag-node" : "")}
                      key={activity.docId}
                      // draggable={draggable}
                    >
                      <Activity
                        isAdmin={isAdmin}
                        planId={planId}
                        activityId={activity.docId}
                        activity={activity}
                        cardView
                      />
                    </div>
                  ))
                }
              </Reorder>
          </div>
        )}
        {selectedTab === 1 && (
          <div className={classes.container}>
            <Empty
              hide={completedActivities.length > 0}
              text={"No activity completed yet!"}
            />
            {completedActivities.map(activity => (
              <div className={classes.activity} key={activity.docId}>
                  <Activity
                    isAdmin={isAdmin}
                    planId={planId}
                    activityId={activity.docId}
                    activity={activity}
                    cardView
                  />
              </div>
            ))}
          </div>
        )}
        {selectedTab === 2 && (
          <div className={classes.container}>
            <Paper className={classes.input}>
              <CreateActivity
                createActivity={activity =>
                  createActivity({ ...activity, isBacklog: true })
                }
              />
            </Paper>
            <Empty
              hide={backLogsActivities.length > 0}
              text={"No activity yet!"}
            />
            {backLogsActivities.map(activity => (
              <div
                key={activity.docId}
                className={classes.activity}
              >
                <Activity
                  isAdmin={isAdmin}
                  planId={planId}
                  activityId={activity.docId}
                  activity={activity}
                  cardView
                />
              </div>
            ))}
          </div>
        )}
      </>
    );
  }
}

export default withStyles(styles)(Activities);
