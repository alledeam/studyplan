import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { format } from "date-fns";
import Typography from "@material-ui/core/Typography";
const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#ae0f0f",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default
  }
}))(TableRow);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
}));

export default function PuchaseHostory({ stats }) {
  const classes = useStyles();
  const [formData, setFormData] = React.useState({
    totalPurchased: 0
  });
  React.useEffect(() => {
    let totalPurchased = stats.tickets.reduce(
      (a, c) => a + parseInt(c.minutes),
      0
    );

    setFormData({ ...formData, totalPurchased });
  }, [stats.tickets, formData]);

  function sortTickets(tickets) {
    return tickets.sort((a, b) => {
      return b.date.toDate() - a.date.toDate();
    });
  }
  return (
    <div className={classes.root}>
      <Typography variant="h4" gutterBottom>
        Total Purchased
      </Typography>
      <Typography variant="h6" gutterBottom>
        Total Minutes :{formData.totalPurchased}
      </Typography>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <StyledTableCell>Date</StyledTableCell>
            <StyledTableCell align="right">Minutes</StyledTableCell>
            <StyledTableCell align="right">Note</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {sortTickets(stats.tickets).map((ticket, index) => {
            return (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row">
                  {format(ticket.date.toDate(), "do MMM, yyy")}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {ticket.minutes}
                </StyledTableCell>
                <StyledTableCell align="right" style={{ maxWidth: "180px" }}>
                  {ticket.note}
                </StyledTableCell>
              </StyledTableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
}
