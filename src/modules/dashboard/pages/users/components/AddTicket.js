import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import MyInput from "../../../../shared/Input";

const INITIAL_STATE = {
  form: {
    minutes: 0,
    note: "",
    date: new Date()
  },
  inValidInputs: {},
  isSubmitted: false
};
class CreateTicket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inValidInputs: {},
      isSubmitted: false,
      form: {
        minutes: 0,
        note: "",
        date: new Date()
      }
    };
  }
  componentDidMount() {
    const { ticket } = this.props;
    if (ticket) {
      this.setState({
        form: {
          minutes: ticket.minutes,
          note: ticket.note,
          date: ticket.date.toDate()
        }
      });
    }
  }
  handleSubmit = e => {
    e.preventDefault();
    const { inValidInputs } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0) {
      return;
    }
    this.setState({ isLoggedIn: true });
    if (this.props.edit) {
      this.props.handleSubmit({
        ticket: this.state.form,
        createdAt: this.props.ticket.createdAt
      });
    } else {
      this.props
        .handleSubmit({
          userId: this.props.user.docId,
          ticket: this.state.form
        })
        .then(data => {
          if (data) {
            this.closeModal();
          }
        });
    }
  };

  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    const { form, inValidInputs } = this.state;
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    } else {
      delete inValidInputs[name];
    }
    this.setState({ form, inValidInputs });
  };
  closeModal = () => {
    this.props.handleClose();
    this.setState({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form
      }
    });
  };
  render() {
    const { open, user } = this.props;
    const { form } = this.state;
    return (
      <div>
        <Dialog
          open={open}
          onClose={this.closeModal}
          PaperComponent={Paper}
          aria-labelledby="dialog-title"
        >
          <div className="dialoge-width">
            <DialogTitle id="dialog-title">
              {this.props.edit
                ? `Edit Ticket for ${(user || {}).username}`
                : `  Add Ticket for ${(user || {}).username}`}
            </DialogTitle>
            <form noValidate onSubmit={this.handleSubmit} method="post">
              <DialogContent>
                <MyInput
                  type="date"
                  label={"Date"}
                  name={"date"}
                  value={form.date}
                  isSubmitted={this.state.isSubmitted}
                  onChange={this.handleInputChange}
                  required
                />
                <MyInput
                  type="number"
                  label={"Minutes"}
                  name={"minutes"}
                  value={form.minutes}
                  isSubmitted={this.state.isSubmitted}
                  onChange={this.handleInputChange}
                  required
                />
                <TextField
                  id="outlined-dense-multiline"
                  name={"note"}
                  label="Note"
                  margin="dense"
                  variant="outlined"
                  multiline
                  style={{ width: "100%" }}
                  rowsMax="18"
                  rows="3"
                  value={form.note}
                  onChange={this.handleInputChange}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.closeModal} color="primary">
                  Cancel
                </Button>
                <Button type="submit" variant="contained" color="primary">
                  {this.props.edit ? `Edit Ticket` : `  Create Ticket`}
                </Button>
              </DialogActions>
            </form>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default CreateTicket;
