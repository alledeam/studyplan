import { makeStyles, withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { differenceInMinutes, format } from "date-fns";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import MuiTableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ActiveIconOn from "@material-ui/icons/CheckBox";
import ActiveIconOff from "@material-ui/icons/CheckBoxOutlineBlank";

import React from "react";
import {
  getPlanActivities,
  getPlanFromPlanId,
  getPlansUser,
  getUsersPlan,
} from "../../../redux/firebase";
import {
  updateTicketForUser,
  deleteTicketForUser,
  setTicketActive,
} from "../../../redux/actions";
import Loader from "../../../../shared/Loader";
import CreateTicket from "../components/AddTicket";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Confirmation from "../../../../shared/Confirmation";
import { connect } from "react-redux";
import { func } from "prop-types";
import { deepCopy } from "../../../../../services/common";
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto",
  },
  table: {
    minWidth: 700,
  },
}));

const TableCell = withStyles({
  root: {
    borderBottom: "none",
    fontSize: "1rem",
  },
})(MuiTableCell);

function UserPurchase(props) {
  const classes = useStyles();
  const [formData, setFormData] = React.useState({
    activities: [],
    tickets: [],
  });
  const [loading, setLoading] = React.useState(true);
  const [open, setOpen] = React.useState(null);
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const [selectedTicket, setSelectedTicket] = React.useState(null);
  const [ticket, setTicket] = React.useState(null);
  React.useEffect(() => {
    let isUnMounted = false;
    const userId = props.user.uid;
    let activities = [];
    let allActivities = [];
    let users = [];

    getUsersPlan(userId).then((plan) => {
      Object.keys(plan || {}).map((planId) => {
        return getPlanActivities(planId).then((ac) => {
          activities.push({ activity: ac, planId });
          return ac;
        });
      });
      Object.keys(plan || {}).map((planId) => {
        return getPlansUser(planId).then((user) => {
          users.push({ user: user, planId });
          return user;
        });
      });
      let plansData = Object.keys(plan || {}).map((planId) => {
        return getPlanFromPlanId(planId).then((ac) => {
          return ac;
        });
      });
      Promise.all(plansData).then((pp) => {
        pp.map((ac) => {
          let activity = activities.find((p) => p.planId === ac.id);

          if (activity) {
            activity.activity.map((act) => {
              if (
                act.isCompleted &&
                act.startTime &&
                act.finishTime &&
                !act.excludedStudent
              ) {
                allActivities.push({
                  activity: deepCopy(act),
                  planId: activity.planId,
                  plan: deepCopy(ac.data),
                  users: {},
                });
              }
              return act;
            });
          }

          return ac;
        });
        allActivities.sort((a, b) => {
          return (
            (b.activity.finishTime.toDate()
              ? b.activity.finishTime.toDate()
              : b.activity.completedAt.toDate()) -
            (a.activity.finishTime
              ? a.activity.finishTime.toDate()
              : a.activity.completedAt.toDate())
          );
        });
        if (!isUnMounted) {
          setFormData({
            ...formData,
            activities: allActivities,
            tickets: props.stats.tickets ? deepCopy(props.stats.tickets) : [],
          });
          setLoading(false);
        }
      });
    });
    return () => {
      isUnMounted = true;
    };
  }, []);
  React.useEffect(() => {
    setFormData({
      ...formData,
      tickets: deepCopy(props.stats.tickets),
    });
  }, [props.stats.tickets]);

  function handleEditTicket(data) {
    props
      .updateTicketForUser({
        userId: props.user.docId,
        oldTicket: ticket,
        newTicket: data.ticket,
      })
      .then(function () {
        closeModal();
      })
      .catch(function (error) {
        closeModal();
      });
  }

  function sortTickets(activities, tickets) {
    let mergerdArray = [];
    let activitiesData = activities.map((act) => {
      act.date = act.activity.startTime;
      act.type = "Consumed";
      act.minutes = diff(act.activity);
      return act;
    });
    let ticketData = tickets.map((t) => {
      t.type = "Purchased";
      return t;
    });
    mergerdArray.push(...activitiesData, ...ticketData);
    return mergerdArray.sort((a, b) => {
      return b.date.toDate() - a.date.toDate();
    });
  }
  function getTotalPurchased(tickets) {
    return Array.isArray(tickets)
      ? tickets.reduce((a, c) => a + parseInt(c.minutes), 0)
      : 0;
  }

  function getCurrent(formData) {
    const { tickets, activities } = formData;
    let totalCurrentPurchase = Array.isArray(tickets)
      ? tickets
          .filter((ticket) => ticket.active)
          .reduce((a, c) => a + parseInt(c.minutes), 0)
      : 0;
    let totalUsage = getTotalConsumed(activities);
    //reducing data
    const sortedtickets = tickets
      .filter((ticket) => ticket.active)
      .sort((a, b) => a.date.toDate() - b.date.toDate());
    if (sortedtickets.length <= 0)
      return {
        purchased: null,
        usage: null,
      };
    for (let i = 0; i < sortedtickets.length; i++) {
      const ticket = sortedtickets[i];
      const minutes = parseFloat(ticket.minutes);
      if (minutes < totalUsage) {
        totalCurrentPurchase -= minutes;
        totalUsage -= minutes;
      } else {
        break;
      }
    }
    return {
      purchased: totalCurrentPurchase,
      usage: totalUsage,
    };
  }
  function getTotalConsumed(activities) {
    return Array.isArray(activities)
      ? activities.reduce((a, c) => {
          if (!c.activity.excludedStudent) {
            return a + diff(c.activity);
          }
          return 0;
        }, 0)
      : 0;
  }
  function diff(activity) {
    if (activity && activity.startTime && activity.finishTime) {
      const d1 = activity.finishTime.toDate();
      const d2 = activity.startTime.toDate();
      d1.setDate(d2.getDate());
      const differenceInMinutesAcivity = differenceInMinutes(d1, d2);
      return differenceInMinutesAcivity > 0 ? differenceInMinutesAcivity : 0;
    }
    return 0;
  }
  function openEditTicketModal(ticket) {
    setSelectedTicket(ticket);
    setTicket(ticket);
    setOpen(1);
  }
  function openDeleteTicketModal(ticket) {
    setSelectedTicket(ticket);
    setOpenConfirm(true);
  }
  function handleDisagree() {
    setSelectedTicket(null);
    setTicket(null);
    setOpenConfirm(false);
  }
  function handleAgree() {
    props
      .deleteTicketForUser({ ticket: selectedTicket, userId: props.user.docId })
      .then(function () {
        setSelectedTicket(null);
        setTicket(null);
        setOpenConfirm(false);
      })
      .catch(function (error) {
        setSelectedTicket(null);
        setTicket(null);
        setOpenConfirm(false);
      });
  }
  function closeModal() {
    setOpen(null);
    setSelectedTicket(null);
    setTicket(null);
  }
  return (
    <div className={classes.root}>
      {props.role === "STUDENT" && (
        <>
          <div className="stats">
            <Typography variant="h5" gutterBottom>
              <span className="f-50">
                {getTotalPurchased(formData.tickets)}
              </span>
              <span className="f-16">Minutes</span>
              <br />
              <span className="f-16">Total Purchased</span>
            </Typography>

            <Typography variant="h5" gutterBottom>
              <span className="f-50">
                {getCurrent(formData).purchased || ""}
              </span>
              <span className="f-16">Minutes</span>
              <br />
              <span className="f-16">Current Purchased</span>
            </Typography>

            <Typography variant="h5" gutterBottom>
              <span className="f-50">{getCurrent(formData).usage || ""}</span>
              <span className="f-16">Minutes</span>
              <br />
              <span className="f-16">Current Consumed</span>
            </Typography>

            <Typography variant="h5" gutterBottom>
              <span className="f-50">
                {getTotalConsumed(formData.activities)}
              </span>
              <span className="f-16">Minutes</span>
              <br />
              <span className="f-16">Total Consumed</span>
            </Typography>
          </div>
          {loading ? (
            <Loader />
          ) : formData.activities.length || formData.tickets.length ? (
            <>
              <Table className={"student-data-list"}>
                <colgroup>
                  <col style={{ width: "25%" }} />
                  <col style={{ width: "15%" }} />
                  <col style={{ width: "60%" }} />
                </colgroup>
                <TableHead>
                  <TableRow style={{ borderBottom: "1px solid black" }}>
                    <TableCell>Date</TableCell>
                    <TableCell>Minutes</TableCell>
                    <TableCell>Activity/Note</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(() => {
                    let sum = 0;
                    const len =
                      formData.activities.length + formData.tickets.length;
                    return sortTickets(
                      formData.activities,
                      formData.tickets
                    ).map((act, index) => {
                      let res = sum;
                      if (act.type === "Purchased") {
                        sum = 0;
                      } else {
                        sum += act.minutes;
                      }
                      return (
                        <TableRow
                          key={index}
                          style={{
                            borderBottom:
                              act.type === "Purchased" && len - 1 !== index
                                ? "solid 1px"
                                : "0px",
                            borderTop: index === 0 ? "solid 1px" : "0px",
                          }}
                        >
                          <TableCell>
                            {format(act.date.toDate(), "do MMMM yyy")}
                          </TableCell>
                          <TableCell className="minutes">
                            {act.minutes}
                          </TableCell>
                          <TableCell>
                            {act.type === "Purchased" ? (
                              <>
                                <span
                                  style={{
                                    width: "80%",
                                    display: "inline-block",
                                  }}
                                >
                                  {act.note}
                                </span>
                                <span
                                  style={{
                                    width: "20%",
                                    display: "inline-block",
                                  }}
                                >
                                  <IconButton
                                    size="small"
                                    onClick={() => openEditTicketModal(act)}
                                  >
                                    <EditIcon />
                                  </IconButton>
                                  <IconButton
                                    size="small"
                                    onClick={() => openDeleteTicketModal(act)}
                                  >
                                    <DeleteIcon />
                                  </IconButton>
                                  <IconButton
                                    size="small"
                                    onClick={() =>
                                      props.setTicketActive({
                                        userId: props.user.docId,
                                        ticket: act,
                                        active: act.active ? false : true,
                                      })
                                    }
                                  >
                                    {act.active ? (
                                      <ActiveIconOn />
                                    ) : (
                                      <ActiveIconOff />
                                    )}
                                  </IconButton>

                                  {selectedTicket && (
                                    <CreateTicket
                                      open={open === 1}
                                      user={props.user}
                                      handleClose={closeModal}
                                      ticket={selectedTicket}
                                      edit={true}
                                      handleSubmit={handleEditTicket}
                                    />
                                  )}
                                </span>
                              </>
                            ) : (
                              <a
                                href={`/dashboard/plans/${act.planId}/${act.activity.docId}`}
                                target="__blank"
                                className="nodecoration"
                              >
                                {act.activity.activity}
                              </a>
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    });
                  })()}
                </TableBody>
              </Table>
            </>
          ) : (
            <Typography variant="body2" className="text-center">
              No stats to show!
            </Typography>
          )}
        </>
      )}
      {openConfirm && (
        <Confirmation
          title="Delete Confirmation ?"
          description="Are you sure you want to delete this ticket ?"
          handleDisagree={handleDisagree}
          handleAgree={handleAgree}
        />
      )}
    </div>
  );
}
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  updateTicketForUser,
  deleteTicketForUser,
  setTicketActive,
})(UserPurchase);
