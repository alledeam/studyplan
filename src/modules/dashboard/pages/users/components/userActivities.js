import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { format, differenceInMinutes } from "date-fns";
import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Loader from "../../../../shared/Loader";
import { deepCopy } from "../../../../../services/common";
import {
  getUsersPlan,
  getPlanActivities,
  getPlanFromPlanId,
  getPlansUser
} from "../../../redux/firebase";
import WithUser from "../../../common/WithUser";
const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#ae0f0f",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default
  }
}))(TableRow);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
}));
function diff(activity) {
  if (activity.startTime && activity.finishTime) {
    const d1 = activity.finishTime.toDate();
    const d2 = activity.startTime.toDate();
    d1.setDate(d2.getDate());
    const diff = differenceInMinutes(d1, d2);
    let hr = parseInt(diff / 60);
    let min = diff - hr * 60;
    if (hr < 10) {
      hr = `0${hr}`;
    }
    if (min < 10) {
      min = `0${min}`;
    }
    return `${hr}:${min}`;
  }
  return "_:_";
}

export default function UserActivities(props) {
  const classes = useStyles();
  const [formData, setFormData] = React.useState({
    activities: [],
    allUniqueUsers: [],
    monthArray: [],
    totalConsumedMinutes: 0,
    monthlyUsers: [],
    filteredUsers: [],
    teacherCompletedActivities: []
  });
  const [loading, setLoading] = React.useState(true);
  React.useEffect(() => {
    let isUnMounted = false;
    const userId = props.user.uid;
    let activities = [];
    let allActivities = [];
    let users = [];
    let allUniqueUsers = [];

    getUsersPlan(userId).then(plan => {
      Object.keys(plan || {}).map(planId => {
        return getPlanActivities(planId).then(ac => {
          activities.push({ activity: ac, planId });
          return ac;
        });
      });
      Object.keys(plan || {}).map(planId => {
        return getPlansUser(planId).then(user => {
          users.push({ user: user, planId });
          return user;
        });
      });
      let plansData = Object.keys(plan || {}).map(planId => {
        return getPlanFromPlanId(planId).then(ac => {
          return ac;
        });
      });
      Promise.all(plansData).then(pp => {
        pp.map(ac => {
          let activity = activities.find(p => p.planId === ac.id);
          if (activity) {
            activity.activity.map(act => {
              if (
                act.isCompleted &&
                act.startTime &&
                act.finishTime &&
                !act.excludedTeacher
              ) {
                const push =
                  props.role === "TEACHER"
                    ? act.completedBy === props.user.uid
                    : true;
                if (push && !act.excludedTeacher) {
                  allActivities.push({
                    activity: deepCopy(act),
                    planId: activity.planId,
                    plan: deepCopy(ac.data),
                    users: {}
                  });
                }
              }
              return act;
            });
          }

          return ac;
        });
        users.map(u => {
          return u.user.map(us => {
            if (!allUniqueUsers.find(user => user.user === us.docId)) {
              return allUniqueUsers.push({
                user: us.docId,
                planId: u.planId,
                activities: [],
                userDetails: {}
              });
            }
            return us;
          });
        });
        // NbxjVsAqTcKdbJwtzteq
        let usersArr = allActivities.map(el => {
          let user = users.find(u => u.planId === el.planId);
          if (user) {
            el.users = user.user;
          }
          return el;
        });

        var planIds = [];
        let totalConsumedMinutes = 0;
        allActivities.sort((a, b) => {
          return b.activity.startTime.toDate() - a.activity.startTime.toDate();
        });
        let arr = allActivities.map(el => {
          if (planIds.includes(el.planId)) {
            el.plan.title = "";
          } else {
            planIds.push(el.planId);
          }
          totalConsumedMinutes =
            totalConsumedMinutes + calculateTotalMinutes(el.activity);
          return el;
        });
        let startDate, endDate;
        if (arr.length && props.role === "TEACHER") {
          startDate = ((arr[0].activity || {}).startTime || {}).toDate();
          endDate = ((arr[0].activity || {}).startTime || {}).toDate();
          arr.map(a => {
            if (a.activity.startTime.toDate() < startDate) {
              startDate = a.activity.startTime.toDate();
            }
            if (a.activity.startTime.toDate() > endDate) {
              endDate = a.activity.startTime.toDate();
            }
            return a;
          });
        }

        let start = new Date(startDate);
        let end = new Date(endDate);
        let monthArray = [];
        start.setDate(1);
        end.setDate(1);
        while (
          start.getMonth() <= end.getMonth() ||
          start.getFullYear() <= end.getFullYear()
        ) {
          monthArray.unshift({
            month: new Date(start),
            activities: [],
            user: {},
            totalMinutes: 0
          });

          start.setMonth(start.getMonth() + 1);
        }
        const teacherCompletedActivities = allActivities
          .filter(act => {
            return (
              act.activity.completedBy === props.user.uid &&
              !act.activity.excludedTeacher &&
              act.activity.isCompleted
            );
          })
          .sort((a, b) => {
            return (
              (b.activity.finishTime
                ? b.activity.finishTime.toDate()
                : b.activity.completedAt.toDate()) -
              (a.activity.finishTime
                ? a.activity.finishTime.toDate()
                : a.activity.completedAt.toDate())
            );
          });
        teacherCompletedActivities.map(act => {
          return allUniqueUsers.map(us => {
            if (act.users.find(u => u.docId === us.user)) {
              return us.activities.push(deepCopy(act.activity));
            }
            return us;
          });
        });
        if (!isUnMounted) {
          setFormData({
            ...formData,
            activities: arr,
            totalConsumedMinutes,
            allUniqueUsers,
            monthArray,
            teacherCompletedActivities
          });
          setLoading(false);

          if (props.role && props.role === "TEACHER") {
            filterUsers();
          }
        }
      });
    });
    return () => {
      isUnMounted = true;
    };
  }, []);
  React.useEffect(() => {
    if (
      props.users.length &&
      formData.activities.length &&
      formData.allUniqueUsers.length
    ) {
      formData.allUniqueUsers.map(user => {
        let userData = props.users.find(us => us.uid === user.user);
        if (userData) {
          user.userDetails = deepCopy(userData);
        }
        return user;
      });
      if (props.role && props.role === "TEACHER") {
        filterUsers();
      }
    }
  }, [props.users, formData.activities, formData.allUniqueUsers]);

  function calculateTotalMinutes(activity) {
    if (
      activity.startTime &&
      activity.finishTime &&
      !activity.excludedTeacher
    ) {
      const d1 = activity.finishTime.toDate();
      const d2 = activity.startTime.toDate();
      d1.setDate(d2.getDate());
      const diff = differenceInMinutes(d1, d2);
      return diff;
    }
    return 0;
  }

  function filterUsers() {
    if (formData.allUniqueUsers && formData.allUniqueUsers.length) {
      const monthArray = formData.monthArray.map(month => {
        const monthlyActivities = formData.teacherCompletedActivities
          .filter(act => {
            let mon = new Date(month.month);
            let date = act.activity.startTime.toDate();
            return (
              mon.getMonth() === date.getMonth() &&
              mon.getFullYear() === date.getFullYear()
            );
          })
          .map(activity => {
            const users = activity.users
              .map(user => {
                const u = props.users.find(u => u.docId === user.docId);
                return u;
              })
              .filter(user => {
                return !(user || {}).role || user.role === "STUDENT";
              });
            return {
              ...activity,
              minutes: calculateTotalMinutes(activity.activity),
              users: users
            };
          });

        const totalMinutes = monthlyActivities.reduce((sum, act) => {
          return sum + calculateTotalMinutes(act.activity);
        }, 0);
        return {
          ...month,
          activities: monthlyActivities,
          totalMinutes
        };
      });
      setFormData({
        ...formData,
        monthlyUsers: monthArray
      });
    }
  }
  return (
    <div className={classes.root}>
      <>
        {loading ? (
          <Loader />
        ) : (
          props.role === "TEACHER" && (
            <React.Fragment key="teacher">
              {formData.monthlyUsers.length ? (
                <React.Fragment key="teacher1">
                  <div>
                    <Typography variant="h6" gutterBottom>
                      Monthly Activities
                    </Typography>
                  </div>
                  {formData.monthlyUsers.map((user, index) => (
                    <React.Fragment key={`frag-${index}`}>
                      {user.activities.length ? (
                        <ExpansionPanel key={`pannel-${index}`}>
                          <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                            key={`summary-${index}`}
                          >
                            <div
                              style={{
                                width: "100%",
                                display: "flex",
                                justifyContent: "space-between"
                              }}
                            >
                              <Typography className={classes.heading}>
                                {format(user.month, "MMMM yyy")}
                              </Typography>
                              <Typography className={classes.heading}>
                                {` Total Minutes: ${user.totalMinutes}`}
                              </Typography>
                            </div>
                          </ExpansionPanelSummary>
                          <ExpansionPanelDetails>
                            <Table className={classes.table}>
                              <TableHead>
                                <TableRow>
                                  <StyledTableCell>Date</StyledTableCell>
                                  <StyledTableCell>User</StyledTableCell>
                                  <StyledTableCell align="right">
                                    Minutes
                                  </StyledTableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {user.activities.length &&
                                  user.activities.map((data, i) => {
                                    const act = data.activity;
                                    return (
                                      <StyledTableRow key={act.docId}>
                                        <StyledTableCell
                                          component="th"
                                          scope="row"
                                        >
                                          {format(
                                            act.startTime.toDate(),
                                            "do MMMM  yyy"
                                          )}
                                        </StyledTableCell>
                                        <StyledTableCell>
                                          <a
                                            href={`/dashboard/plans/${data.planId}/${act.docId}`}
                                            target="__blank"
                                            className={"nodecoration"}
                                          >
                                            {data.users.length === 0
                                              ? "Self"
                                              : data.users
                                                  .filter(u => u)
                                                  .map(u => u.username)
                                                  .join(", ")}
                                          </a>
                                        </StyledTableCell>
                                        <StyledTableCell align="right">
                                          {calculateTotalMinutes(act)}
                                        </StyledTableCell>
                                      </StyledTableRow>
                                    );
                                  })}
                              </TableBody>
                            </Table>
                          </ExpansionPanelDetails>
                        </ExpansionPanel>
                      ) : null}
                    </React.Fragment>
                  ))}
                </React.Fragment>
              ) : (
                <Typography
                  variant="body2"
                  key={"body2"}
                  className="text-center"
                >
                  No Activity to show!
                </Typography>
              )}
            </React.Fragment>
          )
        )}
      </>
    </div>
  );
}
