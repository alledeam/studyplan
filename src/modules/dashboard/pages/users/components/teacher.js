import React from 'react';
// import clsx from 'clsx';
import PropTypes from 'prop-types';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import { connect } from 'react-redux';
import { createTicket, updateUserRole } from '../../../redux/actions';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import DeleteIcon from "@material-ui/icons/Delete";
import Menu from '@material-ui/core/Menu';
import UserActivities from './userActivities';

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  // { id: "action", numeric: false, disablePadding: false, label: "Action" },
  { id: 'username', numeric: false, disablePadding: false, label: 'Username' },
  { id: 'role', numeric: false, disablePadding: false, label: 'Role' },
  { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
  { id: "deleteUser", numeric: true, disablePadding: false, label: "Delete User Account" },

];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headRows.map((row, index) => (
          <TableCell
            key={row.id}
            style={{ paddingLeft: index === 0 ? '30px' : 'auto' }}
            align={row.numeric ? 'right' : 'left'}
            padding={row.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === row.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: '1 1 100%'
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: '0 0 auto'
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();

  return (
    <Toolbar className={classes.root}>
      <div className={classes.title}>
        <Typography variant='h6' id='tableTitle'>
          Teachers
        </Typography>
      </div>
      <div className={classes.spacer} />
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3)
  },
  paper: {
    width: 'calc(100%- 20px )',
    marginBottom: theme.spacing(2),
    padding: '10px'
  },
  table: {
    minWidth: 750
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  stats: {
    background: 'linear-gradient(0deg, #ae0e0f91, #eae8e8)'
  },
  active: {
    backgroundColor: '#eae4e4'
  }
}));

function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('username');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState(props.final);
  const [showStats, setShowStats] = React.useState({});
  const [openPopper, setOpenPopper] = React.useState(false);
  const [selectedUserRole, setSelectedUserRole] = React.useState(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorRef = React.useRef(null);
  function handleOpenRoleModal(user, event) {
    setSelectedUserRole(user);
    setAnchorEl(event.currentTarget);
    setOpenPopper(1);
  }

  function handleClose() {
    setAnchorEl(null);
    setOpenPopper(null);
  }
  function handleChangeRole(role) {
    setAnchorEl(null);
    setOpenPopper(null);

    props.handleChangeTeacherRole(role, selectedUserRole);
  }
  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0);
  }

  React.useEffect(() => {
    setRows(props.final);
  }, [props.final]);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  function updateShowStats(id) {
    setShowStats({
      ...showStats,
      [id]: !showStats[id]
    });
  }
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar />
        <div className={classes.tableWrapper}>
          <Table
            className={classes.table}
            aria-labelledby='tableTitle'
            size={'medium'}
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;
                  return (
                    <React.Fragment key={row.email}>
                      <TableRow
                        hover
                        tabIndex={-1}
                        className={showStats[row.docId] ? classes.active : ''}
                      >
                        <TableCell
                          component='th'
                          id={labelId}
                          scope='row'
                          onClick={() => updateShowStats(row.docId)}
                        >
                          <IconButton size='small'>
                            {showStats[row.docId] ? (
                              <KeyboardArrowUp />
                            ) : (
                              <KeyboardArrowDown />
                            )}
                          </IconButton>
                          {row.username}
                        </TableCell>
                        <TableCell>
                          {row.role === 'TEACHER' ? (
                            <>
                              <Button
                                ref={anchorRef}
                                aria-controls='menu-list-grow'
                                aria-haspopup='true'
                                onClick={e => {
                                  handleOpenRoleModal(row, e);
                                }}
                              >
                                {row.role || 'N/A'}
                              </Button>
                              <Menu
                                id='fade-menu'
                                anchorEl={anchorEl}
                                keepMounted
                                open={openPopper === 1}
                                onClose={handleClose}
                              >
                                <MenuItem
                                  onClick={() => handleChangeRole('TEACHER')}
                                >
                                  Teacher
                                </MenuItem>
                                <MenuItem
                                  onClick={() => handleChangeRole('STUDENT')}
                                >
                                  Student
                                </MenuItem>
                              </Menu>
                            </>
                          ) : (
                            <Button
                              ref={anchorRef}
                              aria-controls='menu-list-grow'
                              aria-haspopup='true'
                              disabled={true}
                            >
                              {row.role || 'N/A'}
                            </Button>
                          )}
                        </TableCell>
                        <TableCell>{row.email}</TableCell>
                        <TableCell align="right">
                          <IconButton aria-label="Delete User" onClick={()=>props.deleteUserAccount(row.docId)}>
                          <DeleteIcon />
                        </IconButton>
                        </TableCell>
                      </TableRow>
                      {showStats[row.docId] && (
                        <TableRow className={classes.stats} tabIndex={-1}>
                          <TableCell
                            colSpan={5}
                            component='th'
                            id={labelId}
                            scope='row'
                          >
                            <UserActivities
                              user={row}
                              role={'TEACHER'}
                              users={props.users}
                            />
                          </TableCell>
                        </TableRow>
                      )}
                      {/*  */}
                    </React.Fragment>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={7} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[10, 20, 50, 100]}
          component='div'
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page'
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page'
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

const mapStateToProps = state => ({
  userDetails: state.userDetails
});

export default connect(
  mapStateToProps,
  {
    createTicket,
    updateUserRole
  }
)(EnhancedTable);
