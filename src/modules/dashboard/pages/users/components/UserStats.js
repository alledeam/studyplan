import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";

import TableRow from "@material-ui/core/TableRow";
import { differenceInMinutes } from "date-fns";
import Tooltip from "@material-ui/core/Tooltip";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

import {
  getUserTicket,
  getStudyPlanUsers,
  getLoginId,
  getPlansUser,
  getPlanActivities,
  getRole,
  getUsersPlan,
  getPlanFromPlanId,
} from "../../../redux/firebase";
import { deepCopy } from "../../../../../services/common";
import CircularIndeterminate from "../../../../shared/Loader";

const StyledTableRow = withStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto",
  },
  table: {
    minWidth: 700,
  },
}));

function promisedPlanActivities(plans) {
  return new Promise((resolve) => {
    const activities = [];
    return Object.keys(plans || {}).map(async (planId) => {
      let acts = await getPlanActivities(planId);
      activities.push({ activity: acts, planId });
      return resolve(activities);
    });
  });
}

function UserStats(props) {
  // const userId = props.userId;
  const [formData, setFormData] = React.useState({
    users: [],
    planUsers: [],
  });

  const [consumedTickets, setConsumedTickets] = React.useState(0);

  const [isLoading, setIsLoading] = React.useState(true);

  const listenes = [];
  function getTotalConsumed(activities) {
    return Array.isArray(activities)
      ? activities.reduce((a, c) => {
          if (!c.activity.excludedStudent) {
            return a + diff(c.activity);
          }
          return 0;
        }, 0)
      : 0;
  }

  function diff(activity) {
    if (activity && activity.startTime && activity.finishTime) {
      const d1 = activity.finishTime.toDate();
      const d2 = activity.startTime.toDate();
      d1.setDate(d2.getDate());
      const differenceInMinutesAcivity = differenceInMinutes(d1, d2);
      return differenceInMinutesAcivity > 0 ? differenceInMinutesAcivity : 0;
    }
    return 0;
  }
  React.useEffect(() => {
    let allActivities = [];
    let planId = props.plan.docId;

    getPlansUser(planId).then((users) => {
      const promises = users.map(async (u) => {
        const userRoleData = await getRole(u.docId);
        userRoleData.userID = u.docId;
        return userRoleData;
      });
      Promise.all(promises)
        .then((pArr) => {
          const students = pArr.filter((u) => !u.role || u.role === "STUDENT");
          const plans = students.map((s) =>
            getUsersPlan(s.userID).then((data) => {
              return data;
            })
          );
          return plans;
        })
        .then((pa) => {
          return Promise.all(pa).then(async (plans) => {
            plans = plans[0];
            const activities = await promisedPlanActivities(plans);
            let plansData = Object.keys(plans || {}).map((planId) => {
              return getPlanFromPlanId(planId).then((ac) => {
                return ac;
              });
            });
            Promise.all(plansData).then((pp) => {
              pp.map((ac) => {
                let activity = activities.find((p) => p.planId === ac.id);
                if (activity) {
                  activity.activity.map((act) => {
                    if (
                      act.isCompleted &&
                      act.startTime &&
                      act.finishTime &&
                      !act.excludedStudent
                    ) {
                      allActivities.push({
                        activity: deepCopy(act),
                        planId: activity.planId,
                        plan: deepCopy(ac.data),
                        users: {},
                      });
                    }
                    return act;
                  });
                }
                return ac;
              });
              setConsumedTickets(getTotalConsumed(allActivities));
              setIsLoading(false);
            });
          });
        });
    });
  }, [props.completedActs]);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 10000);
    const plan = props.plan;
    let selectedUsers = [];
    const unSubscribeStudyPlanFn = getStudyPlanUsers(
      plan.docId,
      (studyPlanUsers) => {
        let currentUser = props.users.find((p) => p.docId === getLoginId());
        if (currentUser.role === "STUDENT") {
          selectedUsers = props.users.filter((u) => u.docId === getLoginId());
        } else {
          selectedUsers = props.users.filter((u) =>
            studyPlanUsers.find(
              (p) =>
                p.docId === u.docId &&
                u.role !== "TEACHER" &&
                u.role !== "ADMIN"
            )
          );
        }
        if (selectedUsers.length) {
          let savedUsers = formData.users;
          selectedUsers.forEach((user) => {
            const unSubscribeUserTicketFn = getUserTicket(
              user.docId,
              (tickets) => {
                let usages = (tickets.usages || []).map((u) => {
                  u.minutes = calculateTotalMinutes(u);
                  return u;
                });
                let sortedTickets = (tickets.tickets || [])
                  .filter((ticket) => ticket.active)
                  .sort((a, b) => {
                    return b.date.toDate() - a.date.toDate();
                  });
                let sortedUsages = usages.sort((a, b) => {
                  return b.startTime.toDate() - a.startTime.toDate();
                });

                let beforePurchased = getBeforePurchased(sortedTickets);
                let afterPurchased = sortedTickets.length
                  ? sortedTickets[0].minutes
                  : 0;
                let recentDate = sortedTickets[0]
                  ? sortedTickets[0].date
                  : null;
                let beforeConsumed = 0;
                let afterCosnumed = 0;
                sortedUsages.forEach((us) => {
                  if (
                    !recentDate ||
                    recentDate.toDate() - us.startTime.toDate() > 0
                  ) {
                    beforeConsumed = beforeConsumed + us.minutes;
                  } else {
                    afterCosnumed = afterCosnumed + us.minutes;
                  }
                });
                let oldPurchase =
                  parseInt(beforePurchased) - parseInt(beforeConsumed);
                let currentTicketPurchased = parseInt(afterPurchased);
                let currentTicketConsumed = afterCosnumed;
                user.currentTicketPurchased = currentTicketPurchased;
                user.currentTicketConsumed = currentTicketConsumed;
                user.oldPurchase = oldPurchase;
                user.tickets = sortedTickets;
                const existedUser = savedUsers.find(
                  (u) => u.docId === tickets.docId
                );
                if (existedUser) {
                  savedUsers = savedUsers.map((u) => {
                    return u.docId === tickets.docId ? user : u;
                  });
                  setFormData({
                    ...formData,
                    users: savedUsers,
                  });
                } else {
                  savedUsers = [...savedUsers, user];
                  setFormData({
                    ...formData,
                    users: savedUsers,
                  });
                }
              }
            );
            listenes.push(unSubscribeUserTicketFn);
          });
        }
      }
    );
    listenes.push(unSubscribeStudyPlanFn);
    return () => {
      listenes.forEach((unSunFn) => unSunFn());
    };
  }, []);

  function calculateTotalMinutes(activity) {
    if (activity.startTime && activity.finishTime) {
      const d1 = activity.finishTime.toDate();

      const d2 = activity.startTime.toDate();

      d1.setDate(d2.getDate());
      const diff = differenceInMinutes(d1, d2);
      return diff;
    }
    return 0;
  }

  function getBeforePurchased(tickets) {
    let t = deepCopy(tickets);
    t.shift();
    return Array.isArray(t)
      ? t.reduce((a, c) => a + parseInt(c.minutes), 0)
      : 0;
  }

  if (isLoading) {
    return <CircularIndeterminate />;
  }

  return (
    <div>
      {formData.users.length ? (
        <>
          {formData.users
            .filter((u) => !u.isDeleted)
            .map((user) => (
              <React.Fragment key={user.docId}>
                <Box border={1}>
                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <p
                        style={{ textAlign: "center" }}
                        className={"ticket-history"}
                      >
                        {" "}
                        {user.username}{" "}
                      </p>
                    </Grid>
                    {user.tickets.length > 0 ? (
                      (() => {
                        let totalCurrentPurchase = user.tickets.reduce(
                          (a, c) => a + parseInt(c.minutes),
                          0
                        );
                        let currentUsage = consumedTickets;
                        for (let i = user.tickets.length - 1; i >= 0; i--) {
                          const ticket = user.tickets[i];
                          if (ticket.minutes < currentUsage) {
                            totalCurrentPurchase -= ticket.minutes;
                            currentUsage -= ticket.minutes;
                          } else break;
                        }
                        return (
                          <>
                            <Grid item xs={12} md={4}>
                              <p
                                style={{ textAlign: "center" }}
                                className={"ticket-history"}
                              >
                                {" "}
                                Current Tickets Purchased{" "}
                                <Tooltip title="current">
                                  <span className="badge badge-green">
                                    {totalCurrentPurchase}
                                  </span>
                                </Tooltip>
                                {"   "}
                                {totalCurrentPurchase - currentUsage ? (
                                  <Tooltip
                                    title={
                                      totalCurrentPurchase - currentUsage > 0
                                        ? `remaining`
                                        : `extra usage`
                                    }
                                  >
                                    <span className="badge badge-blue">
                                      {totalCurrentPurchase - currentUsage > 0
                                        ? ` + ${
                                            totalCurrentPurchase - currentUsage
                                          }`
                                        : ` - ${Math.abs(
                                            totalCurrentPurchase - currentUsage
                                          )}`}
                                    </span>
                                  </Tooltip>
                                ) : (
                                  ""
                                )}
                              </p>
                            </Grid>
                            <Grid item xs={12} md={3}>
                              <p
                                style={{ textAlign: "center" }}
                                className={"ticket-history"}
                              >
                                Current Tickets Consumed{" "}
                                <span className="badge badge-red">
                                  {currentUsage}
                                </span>
                              </p>
                            </Grid>
                          </>
                        );
                      })()
                    ) : (
                      <Grid item xs={12} md={7}>
                        <p
                          style={{ textAlign: "center" }}
                          className={"ticket-history"}
                        >
                          No Tickets
                        </p>
                      </Grid>
                    )}
                    {/*user.currentTicketPurchased !== 0 &&
                    user.currentTicketConsumed !== 0 ? (
                      <>
                        <Grid item xs={12} md={4}>
                          <p
                            style={{ textAlign: "center" }}
                            className={"ticket-history"}
                          >
                            {" "}
                            Current Tickets Purchased{" "}
                            <Tooltip title="current">
                              <span className="badge badge-green">
                                {user.currentTicketPurchased}
                              </span>
                            </Tooltip>
                            {"   "}
                            {user.oldPurchase ? (
                              <Tooltip
                                title={
                                  user.oldPurchase > 0
                                    ? `remaining`
                                    : `extra usage`
                                }
                              >
                                <span className="badge badge-blue">
                                  {user.oldPurchase > 0
                                    ? ` + ${user.oldPurchase}`
                                    : ` - ${Math.abs(user.oldPurchase)}`}
                                </span>
                              </Tooltip>
                            ) : (
                              ""
                            )}
                          </p>
                        </Grid>
                        <Grid item xs={12} md={3}>
                          <p
                            style={{ textAlign: "center" }}
                            className={"ticket-history"}
                          >
                            Current Tickets Consumed{" "}
                            <span className="badge badge-red">
                              {consumedTickets}
                            </span>
                          </p>
                        </Grid>
                      </>
                    ) : (
                      <Grid item xs={12} md={7}>
                        <p
                          style={{ textAlign: "center" }}
                          className={"ticket-history"}
                        >
                          No Tickets
                        </p>
                      </Grid>
                    )*/}
                  </Grid>
                </Box>
              </React.Fragment>
            ))}
        </>
      ) : (
        ""
      )}
    </div>
  );
}

const mapStateToProps = (state) => ({
  users: state.users || [],
  isAdmin: state.isAdmin,
});
export default connect(mapStateToProps, {})(UserStats);
