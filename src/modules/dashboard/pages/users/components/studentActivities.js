import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { format, differenceInMinutes } from 'date-fns';
import Typography from '@material-ui/core/Typography';
import Loader from '../../../../shared/Loader';
import { deepCopy } from '../../../../../services/common';

import {
  getUsersPlan,
  getPlanActivities,
  getPlanFromPlanId,
  getPlansUser
} from '../../../redux/firebase';
const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#ae0f0f',
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default
  }
}))(TableRow);

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
}));
function diff(activity) {
  if (activity.startTime && activity.finishTime) {
    const d1 = activity.finishTime.toDate();
    const d2 = activity.startTime.toDate();
    d1.setDate(d2.getDate());
    const diff = differenceInMinutes(d1, d2);
    let hr = parseInt(diff / 60);
    let min = diff - hr * 60;
    if (hr < 10) {
      hr = `0${hr}`;
    }
    if (min < 10) {
      min = `0${min}`;
    }
    return `${hr}:${min}`;
  }
  return '_:_';
}

export default function UserActivities(props) {
  const classes = useStyles();
  const [formData, setFormData] = React.useState({
    activities: [],
    totalConsumedMinutes: 0,
    filteredUsers: []
  });
  const [loading, setLoading] = React.useState(true);
  React.useEffect(() => {
    let isUnMounted = false;
    const userId = props.user.uid;
    let activities = [];
    let allActivities = [];
    let users = [];
    getUsersPlan(userId).then(plan => {
      Object.keys(plan || {}).map(planId => {
        return getPlanActivities(planId).then(ac => {
          activities.push({ activity: ac, planId });
          return ac;
        });
      });
      Object.keys(plan || {}).map(planId => {
        return getPlansUser(planId).then(user => {
          users.push({ user: user, planId });
          return user;
        });
      });
      let plansData = Object.keys(plan || {}).map(planId => {
        return getPlanFromPlanId(planId).then(ac => {
          return ac;
        });
      });
      Promise.all(plansData).then(pp => {
        pp.map(ac => {
          let activity = activities.find(p => p.planId === ac.id);
          if (activity) {
            activity.activity.map(act => {
              if (
                act.isCompleted &&
                act.startTime &&
                act.finishTime &&
                !act.excludedStudent
              ) {
                const push =
                  props.role === 'TEACHER'
                    ? act.completedBy === props.user.uid
                    : true;
                if (push && !act.excludedStudent) {
                  allActivities.push({
                    activity: deepCopy(act),
                    planId: activity.planId,
                    plan: deepCopy(ac.data),
                    users: {}
                  });
                }
              }
              return act;
            });
          }
          return ac;
        });
        let usersArr = allActivities.map(el => {
          let user = users.find(u => u.planId === el.planId);
          if (user) {
            el.users = user.user;
          }
          return el;
        });
        var planIds = [];
        let totalConsumedMinutes = 0;
        allActivities.sort((a, b) => {
          return b.activity.startTime.toDate() - a.activity.startTime.toDate();
        });
        let arr = allActivities.map(el => {
          if (planIds.includes(el.planId)) {
            el.plan.title = '';
          } else {
            planIds.push(el.planId);
          }
          totalConsumedMinutes =
            totalConsumedMinutes + calculateTotalMinutes(el.activity);
          return el;
        });

        if (!isUnMounted) {
          setFormData({
            ...formData,
            activities: arr,
            totalConsumedMinutes
          });
          setLoading(false);
        }
      });
    });
    return () => {
      isUnMounted = true;
    };
  }, []);

  function calculateTotalMinutes(activity) {
    if (
      activity.startTime &&
      activity.finishTime &&
      !activity.excludedStudent
    ) {
      const d1 = activity.finishTime.toDate();
      const d2 = activity.startTime.toDate();
      d1.setDate(d2.getDate());
      const diff = differenceInMinutes(d1, d2);
      return diff;
    }
    return 0;
  }

  return (
    <div className={classes.root}>
      {loading ? (
        <Loader />
      ) : (
        props.role === 'STUDENT' && (
          <>
            <div>
              <Typography variant='h4' gutterBottom>
                Total consumed
              </Typography>
              <Typography variant='h6' gutterBottom>
                Total Minutes Consumed:{formData.totalConsumedMinutes}
              </Typography>
            </div>
            {formData.activities.length ? (
              <>
                <Table className={classes.table}>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell>Plan</StyledTableCell>
                      <StyledTableCell>Activity</StyledTableCell>
                      {props.role && props.role === 'TEACHER' ? (
                        <StyledTableCell align='right'>
                          Start Time
                        </StyledTableCell>
                      ) : (
                        <StyledTableCell>Date</StyledTableCell>
                      )}
                      <StyledTableCell align='right'>
                        Total Time
                      </StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {formData.activities.map((act, index) => {
                      return (
                        <StyledTableRow key={index}>
                          <StyledTableCell component='th' scope='row'>
                            {act.plan.title}
                          </StyledTableCell>
                          <StyledTableCell style={{ maxWidth: '180px' }}>
                            {act.activity.activity}
                          </StyledTableCell>
                          <StyledTableCell>
                            {format(
                              (
                                act.activity.startTime || act.activity.createdAt
                              ).toDate(),
                              'do MMMM,yyy'
                            )}
                            {props.role === 'TEACHER' &&
                              format(
                                (
                                  act.activity.startTime ||
                                  act.activity.createdAt
                                ).toDate(),
                                ' hh:mm a'
                              )}
                          </StyledTableCell>
                          <StyledTableCell align='right'>
                            {diff(act.activity)}
                          </StyledTableCell>
                        </StyledTableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </>
            ) : (
              <Typography variant='body2'>No tickets to show!</Typography>
            )}
          </>
        )
      )}
    </div>
  );
}
