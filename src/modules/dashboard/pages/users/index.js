import React from 'react';
// import clsx from 'clsx';
import PropTypes from 'prop-types';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';

import { connect } from 'react-redux';
import CreateTicket from './components/AddTicket';
import { createTicket, updateUserRole, deleteUser } from '../../redux/actions';
import { getUsersTickets } from '../../redux/firebase';
// import PurchaseHistory from "./components/PuchaseHostory";
// import UserActivities from "./components/userActivities";
import UserPurchase from './components/userPurchase';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import DeleteIcon from '@material-ui/icons/Delete';
import Confirmation from '../../../shared/Confirmation';

import TeacherTable from './components/teacher';

function desc(a, b, orderBy) {
  if (typeof (a[orderBy] || b[orderBy]) === 'string') {
    const aValue = (a[orderBy] || '').toLowerCase();
    const bValue = (b[orderBy] || '').toLowerCase();
    if (bValue < aValue) {
      return -1;
    }
    if (bValue > aValue) {
      return 1;
    }
  } else {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  { id: 'action', numeric: false, disablePadding: false, label: 'Action' },
  { id: 'username', numeric: false, disablePadding: false, label: 'User' },
  {
    id: 'totalPurchased',
    numeric: true,
    disablePadding: false,
    label: 'Total Purchased'
  },
  { id: 'role  ', numeric: true, disablePadding: false, label: 'Role' },
  { id: 'deleteUser', numeric: true, disablePadding: false, label: 'Delete User Account' },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headRows.map((row, index) => (
          <TableCell
            key={row.id}
            align={row.numeric ? 'right' : 'left'}
            padding={row.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === row.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: '1 1 100%'
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: '0 0 auto'
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();

  return (
    <Toolbar className={classes.root}>
      <div className={classes.title}>
        <Typography variant='h6' id='tableTitle'>
          Students
        </Typography>
      </div>
      <div className={classes.spacer} />
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3)
  },
  paper: {
    width: 'calc(100%- 20px )',
    marginBottom: theme.spacing(2),
    padding: '10px'
  },
  table: {
    minWidth: 750
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  stats: {
    background: 'linear-gradient(0deg, #ae0e0f91, #eae8e8)'
  },
  active: {
    backgroundColor: '#eae4e4'
  }
}));

function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('totalPurchased');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = React.useState(null);
  const [selectedUser, setSelectedUser] = React.useState(null);
  const [rows, setRows] = React.useState(props.users);
  const [teachers, setTeacher] = React.useState(props.users);
  const [tickets, setTickets] = React.useState([]);
  const [showStats, setShowStats] = React.useState({});
  const [openPopper, setOpenPopper] = React.useState(false);
  const [selectedUserRole, setSelectedUserRole] = React.useState(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const anchorRef = React.useRef(null);
  function handleOpenRoleModal(user, event) {
    setSelectedUserRole(user);
    setAnchorEl(event.currentTarget);
    setOpenPopper(1);
  }

  function handleClose() {
    setAnchorEl(null);
    setOpenPopper(null);
  }
  function handleChangeRole(role) {
    setAnchorEl(null);
    setOpenPopper(null);
    props.updateUserRole({ user: selectedUserRole, role });
  }
  function handleChangeTeacherRole(role, selectedUserRole) {
    setAnchorEl(null);
    setOpenPopper(null);
    props.updateUserRole({ user: selectedUserRole, role });
  }
  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0);
  }
  function openCreateTicketModal(user) {
    setSelectedUser(user);
    setOpen(1);
  }
  function closeModal() {
    setOpen(null);
    setSelectedUser(null);
  }
  function mergeUsersAndTickets(usersTickest, users) {
    const students = users.filter(user => {
      return !user.role || user.role === 'STUDENT';
    });
    const final = students
      .map(student => {
        const user = usersTickest.find(u => u.docId === student.docId) || {};

        return {
          ...student,
          ...user,
          role: 'STUDENT',
          totalPurchased: user.tickets
            ? user.tickets.reduce((a, c) => a + parseInt(c.minutes), 0)
            : 0
        };
      })
      .filter(t => t);
    setRows(final);
  }

  function mergeTeachersAndTickets(usersTickest, users) {
    const teachers = users.filter(user => {
      return user.role && user.role !== 'STUDENT';
    });
    const final = teachers
      .map(teacher => {
        const user = usersTickest.find(u => u.docId === teacher.docId) || {};

        return {
          ...teacher,
          ...user,
          role: teacher.role,
          totalPurchased: user.tickets
            ? user.tickets.reduce((a, c) => a + parseInt(c.minutes), 0)
            : 0
        };
      })
      .filter(t => t);
    setTeacher(final);
  }
  React.useEffect(() => {
    const unSubscribeFn = getUsersTickets(usersTickest => {
      setTickets(usersTickest);

      mergeUsersAndTickets(usersTickest, props.users);
      mergeTeachersAndTickets(usersTickest, props.users);
      return function() {
        unSubscribeFn();
      };
    });
  }, [props.users]);

  React.useEffect(() => {
    mergeUsersAndTickets(tickets, props.users);
    mergeTeachersAndTickets(tickets, props.users);
    // setRows(props.users);
  }, [props.users, tickets]);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    function updateShowStats(id) {
      setShowStats({
        ...showStats,
        [id]: !showStats[id]
      });
    }

  function deleteUserAccount() {
    const userId = openConfirm;
    const user = props.users.find(u=>u.docId === userId);
    props.deleteUser(user);
    setOpenConfirm(false);

  }
  function deleteConfirmationText(){
    const userId = openConfirm;
    const user = props.users.find(u=>u.docId === userId);
    return user ? `Are you sure you want to delete '${user.username}' having email '${user.email}' ?`: '';
  }
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar />
        <div className={classes.tableWrapper}>
          <Table
            className={classes.table}
            aria-labelledby='tableTitle'
            size={'medium'}
            responsive='stacked'
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;
                  return (
                    <React.Fragment key={row.email}>
                      <TableRow
                        hover
                        tabIndex={-1}
                        className={showStats[row.docId] ? classes.active : ''}
                      >
                        <TableCell>
                          <IconButton
                            size='small'
                            // disabled={row.totalPurchased === 0}
                            onClick={() => updateShowStats(row.docId)}
                          >
                            {showStats[row.docId] ? (
                              <KeyboardArrowUp />
                            ) : (
                              <KeyboardArrowDown />
                            )}
                          </IconButton>
                          <IconButton
                            size='small'
                            onClick={() => openCreateTicketModal(row)}
                          >
                            <AddIcon />
                          </IconButton>
                        </TableCell>
                        <TableCell component='th' id={labelId} scope='row'>
                          {row.username}
                          <br />
                          <Typography
                            variant='body2'
                            gutterBottom
                            style={{ color: 'grey' }}
                          >
                            {row.email}
                          </Typography>
                        </TableCell>
                        <TableCell align='right'>
                          {row.totalPurchased}
                        </TableCell>
                        <TableCell align='right'>
                        {row.role === 'ADMIN' ? (
                            'ADMIN'
                          ) : (
                          <>
                          <Button
                            ref={anchorRef}
                            aria-controls='menu-list-grow'
                            aria-haspopup='true'
                            onClick={e => {
                              handleOpenRoleModal(row, e);
                            }}
                          >
                            {row.role || 'N/A'}
                          </Button>
                          <Menu
                            id='fade-menu'
                            anchorEl={anchorEl}
                            keepMounted
                            open={openPopper === 1}
                            onClose={handleClose}
                          >
                            <MenuItem
                              onClick={() => handleChangeRole('TEACHER')}
                            >
                              Teacher
                            </MenuItem>
                            <MenuItem
                              onClick={() => handleChangeRole('STUDENT')}
                            >
                              Student
                            </MenuItem>
                          </Menu>
                          </>
                          )}
                        </TableCell>
                        <TableCell align='right' >
                        <IconButton aria-label='Delete User' onClick={()=>setOpenConfirm(row.docId)}>
                          <DeleteIcon />
                        </IconButton>
                        </TableCell>
                      </TableRow>
                      {showStats[row.docId] && (
                        <TableRow className={classes.stats} tabIndex={-1}>
                          <TableCell
                            colSpan={5}
                            component='th'
                            id={labelId}
                            scope='row'
                          >
                            {/* <PurchaseHistory stats={row}  user={row} role='STUDENT' users={props.users}/>
                            <UserActivities user={row} role='STUDENT' users={props.users}/> */}
                            <UserPurchase
                              stats={row}
                              user={row}
                              role='STUDENT'
                              users={props.users}
                            />
                          </TableCell>
                        </TableRow>
                      )}
                    </React.Fragment>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={7} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[10, 20, 50, 100]}
          component='div'
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page'
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page'
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <TeacherTable
        final={teachers}
        handleChangeTeacherRole={handleChangeTeacherRole}
        users={props.users}
        deleteUserAccount={setOpenConfirm}
      />
      <CreateTicket
        open={open === 1}
        user={selectedUser}
        handleClose={closeModal}
        handleSubmit={props.createTicket}
      />
      {openConfirm && (
        <Confirmation
          title='Delete user account?'
          description={deleteConfirmationText()}
          handleDisagree={()=>setOpenConfirm(false)}
          handleAgree={deleteUserAccount}
        />
      )}
    </div>
  );
}

const mapStateToProps = state => ({

  users: state.users.filter(u=>!u.isDeleted).map(user => ({
    ...user,
    totalBalance: 0,
    totalConsumed: 0,
    totalPurchased: 0
  })),
  userDetails: state.userDetails
});

export default connect(
  mapStateToProps,
  {
    createTicket,
    updateUserRole,
    deleteUser
  }
)(EnhancedTable);
