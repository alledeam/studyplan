import React from "react";
import { Redirect } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { getActivityById } from "../../redux/firebase";

import Activity from "./components/Activity";
import Comments from "./components/Comments";
import Loader from "../../../shared/Loader";
import {
  setDraggableEle
} from "../../redux/actions";
const useStyles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    margin: "auto",
    width: "100%"
  }
});

class ActivityPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      planId: "",
      activityId: "",
      activity: null,
      loading: true,
      inValidId: false,
      activities: [],
      expanded: !props.cardView
    };
  }
  listerers = [];

  componentDidMount() {
    const params = this.props.location
      ? this.props.location.pathname.split("/").filter(p => p)
      : [];
    const planId = this.props.planId || params[2];
    const activityId = this.props.activityId || params[3];

    this.setState({ planId, activityId });
    if (this.props.activity) {
      this.setState({ activity: this.props.activity, loading: false });
    } else {
      const unSubscribeFn = getActivityById(planId, activityId, activity => {
        if (activity) {
          this.setState({ activity, loading: false });
        } else {
          this.setState({ inValidPlanId: true, loading: false });
        }
      });
      this.listerers.push(unSubscribeFn);
    }
  }
  componentWillUnmount() {
    this.listerers.map(unSubscribeFn => unSubscribeFn());
  }
  handleExpandClick = () => {
    this.setState({ expanded : !this.state.expanded})
  };
  render() {
    const { classes, cardView, setDraggableEle, isAdmin } = this.props;
    const { planId, activityId, activity, loading, inValidPlanId, expanded } = this.state;

    if (inValidPlanId) {
      return <Redirect to={`/dashboard/plans/${planId}`} />;
    }
    if (loading) {
      return <Loader />;
    }
    return (
      <div className={classes.root}>
        <Activity
          isAdmin={isAdmin}
          setDraggableEle={setDraggableEle}
          planId={planId}
          activityId={activityId}
          activity={this.props.activity || activity}
          cardView={cardView}
          expanded={expanded}
          handleExpandClick={this.handleExpandClick}
        />
        {expanded && <Comments planId={planId} activityId={activityId} setDraggableEle={setDraggableEle} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default compose(
  withStyles(useStyles),
  connect(
    mapStateToProps,
    { setDraggableEle }
  )
)(ActivityPage);
