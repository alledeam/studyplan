import React from "react";
import clsx from "clsx";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Badge from "@material-ui/core/Badge";
import CommentIcon from "@material-ui/icons/Comment";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import UnPinIcon from "../../../../../assets/images/pin.svg";
import PinIcon from "../../../../../assets/images/pinned.svg";
import CheckCircle from "@material-ui/icons/CheckCircle";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import Avatar from "../../../common/Avatar";
import WithUser from "../../../common/WithUser";
// import UpdateActivity from "./UpdateActivity";
import DateTime from "./DateTime";
import Text from "../../../common/Text";
import { timeAgo } from "../../../redux/util";
import { format, differenceInMinutes } from "date-fns";
import { compose } from "redux";
import { connect } from "react-redux";
import Tooltip from "@material-ui/core/Tooltip";

import {
  updateActivityStatus,
  updateActivityPinStatus,
  updateActivity,
  deleteActivity,
  getPlanUsers,
} from "../../../redux/firebase";

import { finishTimeError } from "../../../redux/actions";

const useStyles = (theme) => ({
  card: {
    width: "100%",
  },
  bottomActions: {
    justifyContent: "space-between",
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
});

class Activity extends React.Component {
  constructor() {
    super();
    this.state = {
      open: null,
      anchorEl: null,
      users: [], //need to be deleted
    };
  }

  handleClickOpen = (open) => {
    this.setState({ open });
  };

  handleClose = () => {
    this.setState({ open: null });
  };
  setMenu = (event) => {
    this.setState({ anchorEl: event ? event.currentTarget : null });
  };
  edit = () => {
    this.setMenu();
    this.setState({ open: 1 });
  };
  delete = () => {
    const { planId, activityId } = this.props;
    this.setMenu();
    deleteActivity(planId, activityId);
  };

  diff = (activity) => {
    if (activity.startTime && activity.finishTime) {
      const d1 = activity.finishTime.toDate();
      const d2 = activity.startTime.toDate();
      d1.setDate(d2.getDate());
      const diff = differenceInMinutes(d1, d2);
      let hr = parseInt(diff / 60);
      let min = diff - hr * 60;
      if (hr < 10) {
        hr = `0${hr}`;
      }
      if (min < 10) {
        min = `0${min}`;
      }
      return `${hr}:${min}`;
    }
    return "_:_";
  };
  updateActivity = () => {
    const { planId, activityId, activity, isAdmin } = this.props;
    if (!activity.isCompleted || isAdmin) {
      if (!activity.finishTime) {
        return this.props.finishTimeError();
      }
      updateActivityStatus(planId, activityId, !activity.isCompleted);
    }
  };

  clicked = (activity) => {
    let planId = "gm85knWrZOCHWJ3rznfS";
    const unSubscribeFn = getPlanUsers(planId, (data) => {
      this.setState(() => ({
        users: data,
      }));
      return function () {
        unSubscribeFn();
      };
    });
  };
  render() {
    const {
      classes,
      planId,
      activityId,
      activity,
      expanded,
      cardView,
      handleExpandClick,
      setDraggableEle,
      isAdmin,
      isTeacher,
    } = this.props;
    const { open, anchorEl } = this.state;
    const dragClass = !expanded ? " drag-from-here disable-select" : "";

    return (
      <>
        <Card
          onClick={() => setDraggableEle(true)}
          className={classes.card + dragClass}
          style={{ borderRadius: expanded ? "5px 5px 0px 0px" : "5px" }}
        >
          <CardHeader
            avatar={<Avatar userId={activity.ownerId} />}
            action={
              (!activity.isCompleted ||
                (activity.isCompleted && (isAdmin || isTeacher))) && (
                <Tooltip title="Actions">
                  <IconButton aria-label="Settings" onClick={this.setMenu}>
                    <MoreVertIcon />
                  </IconButton>
                </Tooltip>
              )
            }
            title={
              <WithUser
                userId={activity.ownerId}
                render={(user) => <span>{user.username}</span>}
              />
            }
            subheader={
              <span>
                {activity.isCompleted
                  ? activity.isBacklog
                    ? "from Current Needs"
                    : "from Activities"
                  : "Open"}
              </span>
            }
          />
          <CardContent>
            <Text text={activity.activity} />
            {cardView && <br />}
            {activity.isCompleted && (
              <WithUser
                userId={activity.completedBy}
                render={(user) => (
                  <span className="completed-by">
                    {" "}
                    completed by {user.username} {user.role}
                  </span>
                )}
              ></WithUser>
            )}
            {cardView && <br />}
            {cardView && (
              <Link
                to={`/dashboard/plans/${planId}/${activityId}`}
                style={{ marginTop: "10px" }}
              >
                Open as a page
              </Link>
            )}
          </CardContent>
          <CardActions
            className={classes.bottomActions + " mobile-d-coulmn"}
            disableSpacing
          >
            <div className="flex-space-between pl-10 mobile-d-coulmn grey lh-3">
              <div>
                Date{" "}
                {format(
                  (activity.startTime || activity.createdAt).toDate(),
                  "do MMM, yyy"
                )}
              </div>
              <div>
                Start Time{" "}
                {format(
                  (activity.startTime || activity.createdAt).toDate(),
                  "hh:mm a"
                )}
              </div>
              <div>
                Finish Time{" "}
                {activity.finishTime
                  ? format(activity.finishTime.toDate(), "hh:mm a")
                  : "_:_"}
              </div>
              <div>{this.diff(activity)}</div>
              <div />
            </div>
            <div>
              <>
                <IconButton
                  aria-label="completed"
                  color={activity.isCompleted ? "primary" : "default"}
                  onClick={this.updateActivity}
                  disabled={
                    this.props.isStudent || (activity.isCompleted && !isAdmin)
                  }
                >
                  <Badge color="primary">
                    {activity.isCompleted ? (
                      <Tooltip title="click to incomplete">
                        <CheckCircle />
                      </Tooltip>
                    ) : (
                      <Tooltip title="click to complete">
                        <CheckCircleOutline />
                      </Tooltip>
                    )}
                  </Badge>
                </IconButton>
              </>

              {!activity.isBacklog && !activity.isCompleted && (
                <IconButton
                  aria-label="pin"
                  color={activity.isPinned ? "primary" : "default"}
                  onClick={() =>
                    updateActivityPinStatus(
                      planId,
                      activityId,
                      !activity.isPinned
                    )
                  }
                >
                  <Tooltip
                    title={
                      activity.isPinned ? "click to unpin" : "click to pin"
                    }
                  >
                    <Badge color="primary">
                      <img
                        alt="pin-activity"
                        className={"pin-img"}
                        src={activity.isPinned ? PinIcon : UnPinIcon}
                      />
                    </Badge>
                  </Tooltip>
                </IconButton>
              )}
              <Tooltip title="comments">
                <IconButton aria-label="comments">
                  <Badge badgeContent={activity.totalComments} color="primary">
                    <CommentIcon />
                  </Badge>
                </IconButton>
              </Tooltip>
              {cardView && (
                <Tooltip
                  title={
                    expanded
                      ? "collapse comments pannel"
                      : "expand comments pannel"
                  }
                >
                  <IconButton
                    className={clsx(classes.expand, {
                      [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="Show more"
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                </Tooltip>
              )}
            </div>
          </CardActions>
        </Card>
        {/* <UpdateActivity
          open={open===1}
          handleClose={this.handleClose}
          activity={activity}
          updateActivity={data => {
            updateActivity(planId, activityId, data);
          }}
        /> */}
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={() => this.setMenu()}
        >
          <MenuItem
            onClick={() => {
              this.edit();
            }}
          >
            Edit
          </MenuItem>
          <MenuItem onClick={this.delete}>Delete</MenuItem>
        </Menu>
        {open === 1 && (
          <DateTime
            open={true}
            isAdmin={isAdmin}
            activity={activity}
            isTeacher={this.props.isTeacher}
            handleClose={this.handleClose}
            updateActivity={(data) => updateActivity(planId, activityId, data)}
            updateActivityStatus={() =>
              updateActivityStatus(planId, activityId, true)
            }
            setDraggableEle={setDraggableEle}
            planId={planId}
            activityId={activityId}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isTeacher: state.userDetails.role === "TEACHER" ? true : false,
  isStudent: state.userDetails.role === "STUDENT" ? true : false,
});

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps, { finishTimeError })
)(Activity);
