import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { format } from 'date-fns';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Paper from '@material-ui/core/Paper';
import AddComment from './AddComment';
import Avatar from '../../../common/Avatar';
import WithUser from '../../../common/WithUser';
import Loader from '../../../../shared/Loader';
import { fetchCommnet } from '../../../redux/firebase';
import { timeAgo } from '../../../redux/util';
import Empty from '../../../common/Empty';
import Text from '../../../common/Text';

const styles = theme => ({
  activity: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    marginTop: '20px'
  },

  inline: {
    display: 'inline'
  },
  container: {
    background: 'linear-gradient(#2a4c4624, #2a4c46c4)',
    borderRadius: '0px 0px 5px 5px',
    paddingTop: '2px'
  },
  messageContainer: {
    maxHeight: 'calc(100vh - 165px)',
    minHeight: '300px',
    padding: '20px',
    overflowY: 'auto'
  },
  dragIcon: {
    cursor: 'move'
  }
});

class Comments extends React.Component {
  constructor(props) {
    super(props);
    this.msgDivRef = React.createRef();
    this.state = {
      loading: true,
      comments: []
    };
  }
  listerers = [];
  fetchComments = () => {
    const { planId, activityId } = this.props;
    const unSubscribeFn = fetchCommnet(planId, activityId, comments => {
      if (comments) {
        this.setState({ comments: comments, loading: false }, () => {
          this.scrolltoBottom();
        });
      }
    });
    this.listerers.push(unSubscribeFn);
  };
  componentDidMount() {
    this.fetchComments();
  }
  componentWillUnmount() {
    this.listerers.map(unSubscribeFn => unSubscribeFn());
  }
  scrolltoBottom = () => {
    this.msgDivRef.current.scrollIntoView({ behavior: 'smooth' });
  };
  render() {
    const { classes, planId, activityId, setDraggableEle } = this.props;
    const { loading, comments } = this.state;

    return (
      <div className={classes.container}>
        <div className={classes.messageContainer}>
          {loading && <Loader />}
          <Empty
            hide={!(!loading && comments.length === 0)}
            text={'No comment yet!'}
          />
          {comments.map(comment => (
            <Paper className={classes.activity} key={comment.docId}>
              <List>
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <Avatar userId={comment.userId} />
                  </ListItemAvatar>
                  <ListItemText
                    // primary=""
                    secondary={
                      <React.Fragment>
                        <Text text={comment.text} component="span" />
                        <br />
                        <WithUser
                          userId={comment.userId}
                          render={user => (
                            <span>
                              {user.username},
                              {format(
                                comment.createdAt.toDate(),
                                'do MMMM  yyy'
                              )}
                            </span>
                          )}
                        />
                      </React.Fragment>
                    }
                  />
                </ListItem>
              </List>
            </Paper>
          ))}
          <div ref={this.msgDivRef} />
        </div>
        <AddComment
          setDraggableEle={setDraggableEle}
          planId={planId}
          activityId={activityId}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Comments);
