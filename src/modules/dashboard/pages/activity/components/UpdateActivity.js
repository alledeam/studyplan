import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import CreateActivity from "../../study-plans/components/CreateActivity";

class updateActivity extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {}
  render() {
    const { handleClose, open, activity, updateActivity } = this.props;
    return (
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          PaperComponent={Paper}
          aria-labelledby="dialog-title"
        >
          <div className="dialoge-width">
            <DialogTitle id="dialog-title">Update Activity</DialogTitle>
            <form noValidate onSubmit={this.handleSubmit} method="post">
              <DialogContent>
                <CreateActivity
                  activity={activity.activity}
                  createActivity={a => {
                    updateActivity(a);
                    handleClose();
                  }}
                  edit
                />
              </DialogContent>
            </form>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default updateActivity;
