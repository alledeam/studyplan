import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import MyInput from "../../../../shared/Input";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import AddComment from "./AddComment";

class CreateTicket extends React.Component {
  constructor(props) {
    super(props);
    const data = props.activity || {};
    const startTime = (data.startTime || data.createdAt).toDate();
    let finishTime = data.finishTime
      ? data.finishTime.toDate()
      : (data.startTime || data.createdAt).toDate();
    finishTime = new Date(finishTime.setDate(startTime.getDate()));
    this.state = {
      form: {
        activity: data.activity || "",
        startTime,
        finishTime,
        date: startTime,
        excludedStudent: data.excludedStudent || false,
        excludedTeacher: data.excludedTeacher || false,
      },
      inValidInputs: {},
      showError: false,
      isSubmitted: false,
    };
  }
  getFinalTime = () => {
    const { form } = this.state;
    const date = new Date(form.date);
    const st = new Date(form.startTime);
    const ft = new Date(form.finishTime);
    return {
      startTime: new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        st.getHours(),
        st.getMinutes()
      ),
      finishTime: new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        ft.getHours(),
        ft.getMinutes()
      ),
      activity: form.activity,
      excludedTeacher: form.excludedTeacher,
      excludedStudent: form.excludedStudent,
    };
  };
  handleSubmit = (e) => {
    e.preventDefault();
    const { inValidInputs, showError } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0 || showError) {
      return;
    }
    this.props.updateActivity(this.getFinalTime()).then((data) => {
      if (data) {
        this.closeModal();
      }
    });
  };
  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    console.log({ name, value, isInvalid });
    const { form, inValidInputs } = this.state;
    const { isAdmin, isTeacher } = this.props;
    const isStudent = !(isAdmin || isTeacher);
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    } else {
      delete inValidInputs[name];
    }
    this.setState({ form, inValidInputs }, () => {
      const { form } = this.state;
      const error = isStudent
        ? false
        : form.startTime && form.finishTime
        ? new Date(form.startTime) >= new Date(form.finishTime)
        : false;
      this.setState({ showError: error });
    });
  };
  handleChange = (event) => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.name]: event.target.checked,
      },
    });
  };
  closeModal = () => {
    this.props.handleClose();
  };

  completeActivity = () => {
    this.props.updateActivity(this.getFinalTime()).then((data) => {
      if (data) {
        this.props.updateActivityStatus().then((data) => {
          if (data) {
            this.closeModal();
          }
        });
      }
    });
  };

  render() {
    const {
      open,
      isAdmin,
      isTeacher,
      setDraggableEle,
      planId,
      activityId,
    } = this.props;
    const { form, showError } = this.state;
    return (
      <div>
        <Dialog
          open={open}
          onClose={this.closeModal}
          PaperComponent={Paper}
          aria-labelledby="dialog-title"
        >
          <div className="dialoge-width">
            <DialogTitle id="dialog-title">Update Activity</DialogTitle>
            <form noValidate onSubmit={this.handleSubmit} method="post">
              <DialogContent>
                <TextField
                  fullWidth
                  onChange={this.handleInputChange}
                  name={"activity"}
                  value={form.activity}
                  id="outlined-dense-multiline"
                  label={"Activity"}
                  margin="dense"
                  variant="outlined"
                  multiline
                  rowsMax="18"
                  rows="4"
                  required
                />
                {(isAdmin || isTeacher) && (
                  <>
                    {" "}
                    <MyInput
                      type="date"
                      label={"Date"}
                      name={"date"}
                      value={form.date}
                      isSubmitted={this.state.isSubmitted}
                      onChange={this.handleInputChange}
                      required
                      showToday={true}
                    />
                    <MyInput
                      type="time"
                      label={"Start Time"}
                      name={"startTime"}
                      value={form.startTime}
                      isSubmitted={this.state.isSubmitted}
                      onChange={this.handleInputChange}
                      // checkValid={val=>form.finishTime > val}
                      // errorMessage={"Finish time must be greater than start time"}
                      required
                    />
                    <MyInput
                      type="time"
                      label={"Finish Time"}
                      name={"finishTime"}
                      value={form.finishTime}
                      isSubmitted={this.state.isSubmitted}
                      onChange={this.handleInputChange}
                      required
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={form.excludedTeacher}
                          name="excludedTeacher"
                          onChange={(e) => this.handleChange(e)}
                        />
                      }
                      label="Exclude Teacher"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={form.excludedStudent}
                          name="excludedStudent"
                          onChange={(e) => this.handleChange(e)}
                        />
                      }
                      label="Exclude Student"
                    />
                  </>
                )}
                {showError && this.state.isSubmitted && (
                  <p className="error-text">
                    {"Finish time must be greater than start time"}
                  </p>
                )}
              </DialogContent>
              <DialogActions>
                <Button onClick={this.closeModal} color="primary">
                  Cancel
                </Button>
                <Button
                  type="submit"
                  variant="contained"
                  color="secondary"
                  disabled={form.activity.length === 0}
                >
                  Update
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  disabled={form.activity.length === 0}
                  onClick={this.completeActivity}
                >
                  Complete
                </Button>
              </DialogActions>
            </form>
            <AddComment
              setDraggableEle={setDraggableEle}
              planId={planId}
              activityId={activityId}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}

export default CreateTicket;
