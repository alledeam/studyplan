import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import SendIcon from "@material-ui/icons/Send";
import { addCommnet } from "../../../redux/firebase";

const styles = theme => ({
  buttonContainer: {
    width: "100%",
    textAlign: "right"
  },
  inputContainer: {
    display: "flex",
    margin: "8px"
  },
  button: {
    height: "100%",
    backgroundColor : "#2a4c46"
  }
});

class AddComment extends React.Component {
  constructor() {
    super();
    this.state = {
      comment: ""
    };
  }
  handleSubmit = () => {
    const { comment } = this.state;
    if (!comment.length > 0) {
      return;
    }
    const { planId, activityId } = this.props;
    addCommnet(planId, activityId, { text : comment })
    .then(()=>{
      this.setState({ comment: "" });
    })
  };
  handleInputChange = ({ target: { value } }) => {
    this.setState({ comment: value });
  };
  componentDidMount() {}
  render() {
    const { classes, setDraggableEle } = this.props;
    const { comment } = this.state;
    return (
      <Paper className={classes.inputContainer}>
        <TextField
          onFocus={()=>setDraggableEle(false)}
          onBlur={()=>setDraggableEle(true)}
          onChange={this.handleInputChange}
          value={comment}
          id={"outlined-dense-multiline" + Math.random()}
          placeholder="Write message"
          variant="outlined"
          multiline
          fullWidth
          rowsMax="18"
          rows="2"
        />
        <div>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.handleSubmit}
          >
            <SendIcon />
          </Button>
        </div>
      </Paper>
    );
  }
}
export default withStyles(styles)(AddComment);
