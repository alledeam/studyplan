import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select";
import MyInput from "../../../../shared/Input";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

const INITIAL_STATE = {
  form: {
    email: "",
    username: "",
    role: "STUDENT"
  },
  inValidInputs: {},
  isSubmitted: false
};

class CreateUser extends React.Component {
  constructor() {
    super();
    this.state = {
      ...INITIAL_STATE
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    const { inValidInputs } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0) {
      return;
    }
    this.setState({ isLoggedIn: true });
    this.props.doRegister({ ...this.state.form }).then(user => {
      if (user) {
        this.setState({ ...INITIAL_STATE });
        this.props.handleClose();
      }
    });
  };
  handleChange = event => {
    this.setState({
      form: {
        ...this.state.form,
        role: event.target.value
      }
    });
  };

  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    const { form, inValidInputs } = this.state;
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    } else {
      delete inValidInputs[name];
    }
    this.setState({
      form: {
        ...this.state.form,
        ...form
      },
      inValidInputs
    });
  };
  componentDidMount() {}
  render() {
    const { handleClose, open } = this.props;
    const { form } = this.state;
    return (
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          PaperComponent={Paper}
          aria-labelledby="dialog-title"
        >
          <div className="dialoge-width">
            <DialogTitle id="dialog-title">Add New User</DialogTitle>
            <form noValidate onSubmit={this.handleSubmit} method="post">
              <DialogContent>
                <FormControl style={{ float: "right" }}>
                  <InputLabel htmlFor="age-simple">Role</InputLabel>
                  <Select
                    fullWidth
                    value={form.role}
                    onChange={this.handleChange}
                    inputProps={{
                      name: "age",
                      id: "age-simple"
                    }}
                  >
                    <MenuItem value={"STUDENT"}>STUDENT</MenuItem>
                    <MenuItem value={"TEACHER"}>TEACHER</MenuItem>
                    <MenuItem value={"ADMIN"}>ADMIN</MenuItem>
                  </Select>
                </FormControl>
                <MyInput
                  type="text"
                  label={"Username"}
                  name={"username"}
                  value={form.username}
                  isSubmitted={this.state.isSubmitted}
                  onChange={this.handleInputChange}
                  required
                />
                <MyInput
                  type="text"
                  label={"Email"}
                  name={"email"}
                  value={form.email}
                  isSubmitted={this.state.isSubmitted}
                  onChange={this.handleInputChange}
                  errorMessage={"Please enter a valid email"}
                  pattern={
                    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                  }
                  required
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button type="submit" variant="contained" color="primary">
                  Create User
                </Button>
              </DialogActions>
            </form>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default CreateUser;
