import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import GroupIcon from "@material-ui/icons/Group";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import LocalActivityIcon from "@material-ui/icons/LocalActivity";
import UserInPlan from "../../../common/UsersInPlan";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexShrink: 0,
    width: "100%"
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  description: {
    borderTop: "solid 1px #e6e9ee"
  },
  paper: {
    padding: "6px 12px",
    borderRadius: "5px 5px 0px 0px",
    color: "rgba(0, 0, 0, 0.54)"
  }
}));

export default function ControlledExpansionPanels({ plans, handleClickOpen }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [selectedPlan, setSelectedPlans] = React.useState(null);
  const [order, setOrder] = React.useState(1);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const handleorder = () => {
    setOrder(-order);
  };

  const orderPlans = plans.sort((a, b)=>{
    if(a.title < b.title) { return -order; }
    if(a.title > b.title) { return order; }
    return 0;
})
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Button className={classes.button} onClick={handleorder}>
          Study Plan Title
          {order === -1 ? <ArrowUpwardIcon /> : <ArrowDownwardIcon />}
        </Button>
      </Paper>
      {orderPlans.map(plan => {
        return (
          <ExpansionPanel
            key={plan.docId}
            expanded={expanded === plan.docId}
            onChange={handleChange(plan.docId)}
          >
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={classes.heading}>
                <Link
                  to={`/dashboard/plans/${plan.docId}`}
                  className="nodecoration"
                >
                  {plan.title}
                </Link>
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.description}>
              <div>
                <Button onClick={() => setSelectedPlans(plan)}>
                  <GroupIcon style={{ color: "grey", marginRight: "10px" }} />{" "}
                  Users :{plan.totalUsers}
                </Button>
                <Button aria-label="comments">
                  <LocalActivityIcon
                    style={{ color: "grey", marginRight: "10px" }}
                  />{" "}
                  Activities :{plan.totalActivities}
                </Button>
                <Button aria-label="comments">
                  <LocalActivityIcon
                    style={{ color: "grey", marginRight: "10px" }}
                  />{" "}
                  Current Needs : {plan.totalBacklogActivities}
                </Button>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        );
      })}
      {selectedPlan && (
        <UserInPlan
          open={Boolean(selectedPlan)}
          handleClose={() => setSelectedPlans(false)}
          plan={selectedPlan}
        />
      )}
    </div>
  );
}
