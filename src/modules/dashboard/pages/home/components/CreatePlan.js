import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import MyInput from "../../../../shared/Input";
import TextField from "@material-ui/core/TextField";

const INITIAL_STATE = {
  form: {
    title: "",
    description: ""
  },
  inValidInputs: {},
  isSubmitted: false,
};
class CreateUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...INITIAL_STATE
    };
    if(props){
      this.update();
    }
  }
  resetState = () => {
    this.setState({ ...INITIAL_STATE });
  };
  handleSubmit = e => {
    e.preventDefault();
    const { inValidInputs } = this.state;
    this.setState({ isSubmitted: true });
    if (Object.keys(inValidInputs).length > 0) {
      return;
    }
    if (this.props.plan) {
      this.props
        .updatePlan(this.props.plan.docId, this.state.form)
        .then(user => {
          this.props.handleClose();
          this.resetState();
        });
    } else {
      this.props.createPlan(this.state.form).then(user => {
        this.props.handleClose();
        this.resetState();
      });
    }
  };

  handleInputChange = ({ target: { name, value, isInvalid } }) => {
    const { form, inValidInputs } = this.state;
    form[name] = value;
    if (isInvalid) {
      inValidInputs[name] = true;
    } else {
      delete inValidInputs[name];
    }
    this.setState({ form, inValidInputs });
  };
  update=()=>{
    const { plan } = this.props;
    if(!plan){
      return;
    }
    this.handleInputChange({ target: { name: "title", value: plan.title } });
    this.handleInputChange({
      target: { name: "description", value: plan.description }
    });
  }
  componentDidUpdate(prevProps) {
    const { plan } = this.props;
    if (!prevProps.plan && plan) {
     this.update();
    }
  }
  componentDidMount() {}
  render() {
    const { handleClose, open, plan } = this.props;
    const { form } = this.state;
    return (
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          PaperComponent={Paper}
          aria-labelledby="dialog-title"
        >
          <div className="dialoge-width">
            <DialogTitle id="dialog-title">
              {plan ? "Update" : "Create"} Plan
            </DialogTitle>
            <form noValidate onSubmit={this.handleSubmit} method="post">
              <DialogContent>
                <MyInput
                  type="text"
                  label={"Title"}
                  name={"title"}
                  value={form.title}
                  isSubmitted={this.state.isSubmitted}
                  onChange={this.handleInputChange}
                  errorMessage={"Please enter a valid title"}
                  required
                />
                <TextField
                  id="outlined-dense-multiline"
                  name={"description"}
                  label="Description"
                  margin="dense"
                  variant="outlined"
                  multiline
                  style={{ width: "100%" }}
                  rowsMax="18"
                  rows="4"
                  value={form.description}
                  onChange={this.handleInputChange}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button type="submit" variant="contained" color="primary">
                  {plan ? "Update" : "Create"} Plan
                </Button>
              </DialogActions>
            </form>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default CreateUser;
