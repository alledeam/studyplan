import React from "react";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import CreateUser from "./components/CreateUser";
import CreatePlan from "./components/CreatePlan";
import Loader from "../../../shared/Loader";
import Empty from "../../common/Empty";
import Plans from "./components/Plans";
import { Link } from "react-router-dom";

import {
  doRegister,
  createPlan,
  updatePlan,
  deletePlan
} from "../../redux/actions";
import { getPlans, getMyPlans, getPlanById } from "../../redux/firebase";

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      open: 0,
      plans: [],
      loading: true,
      selectedPlan: null,
    };
  }
  listenes = [];

  handleSelectedPlan = selectedPlan => {
    this.setState({ selectedPlan });
    this.handleClickOpen(2);
  };

  handleClickOpen = modalID => {
    this.setState({ open: modalID });
  };

  handleClose = () => {
    this.setState({ open: 0, selectedPlan: null });
  };

  componentDidMount() {
    if (this.props.isAdmin) {
      const unSubscribeFn = getPlans(plans => {
        this.setState({ plans, loading: false });
      });
      this.listenes.push(unSubscribeFn);
    } else {
      const unSubscribeFn = getMyPlans(plans => {
        this.setState({ loading: false });
        Object.keys(plans || {}).forEach(planId => {
          const unSubscribePlanFn = getPlanById(planId, data => {
            if (data) {
              this.setState(state => {
                const { plans } = state;
                const plan = { ...data, docId: planId };
                const index = plans.findIndex(p => p.docId === planId);
                if (index > -1) {
                  plans[index] = plan;
                } else {
                  plans.push(plan);
                }
                return { plans };
              });
            }
          });
          this.listenes.push(unSubscribePlanFn);
        });
      });
      this.listenes.push(unSubscribeFn);
    }
  }
  componentWillUnmount() {
    this.listenes.map(unSubscribeFn => unSubscribeFn());
  }

  render() {
    const { isAdmin, doRegister, createPlan, updatePlan,userDetails } = this.props;
    const { open, plans, loading, selectedPlan } = this.state;
    return (
      <>
        <Grid container className="text-right">
          {isAdmin && (
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: "10px" }}
              onClick={() => this.handleClickOpen(1)}
            >
              New User
            </Button>
          )}
          {isAdmin && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.handleClickOpen(2)}
              style={{ marginLeft: "10px" }}
            >
              Add Study Plan
            </Button>
          )}
           {isAdmin && (
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: "10px" }}
              className={"manage-users"}
            >
            <Link to="/dashboard/users" className="nodecoration" >Manage Users</Link>
            </Button>
          )}
           {((userDetails.role==="STUDENT")||(userDetails.role==="TEACHER"))&& (
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: "10px" }}
              onClick={() => this.handleClickOpen(1)}
            >
              <Link to="/dashboard/ticket-history" className="nodecoration">{userDetails.role==="TEACHER"?"Show Teaching History":"Get Ticket History"}</Link>
            </Button>
          )}
        </Grid>
        {plans.length > 0 && (
          <Grid item sm={12} md={12}>
            <Plans plans={plans} handleClickOpen={() => {}} />
          </Grid>
        )}
        <Loader hide={!loading} />
        <Empty
          hide={!(!loading && plans.length === 0)}
          text={"No plan yet!"}
          color={"#ae0f0f"}
        />
        {isAdmin && (
          <CreateUser
            open={open === 1}
            handleClose={this.handleClose}
            doRegister={doRegister}
          />
        )}
        <CreatePlan
          open={open === 2}
          handleClose={this.handleClose}
          isAdmin={isAdmin}
          createPlan={createPlan}
          updatePlan={updatePlan}
          plan={selectedPlan}
        />
      </>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(
  mapStateToProps,
  { doRegister, createPlan, updatePlan, deletePlan }
)(Home);
