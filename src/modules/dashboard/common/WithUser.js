import React from "react";
import { connect } from "react-redux";

function WithUser({ user, render }) {
  return render(user);
}

const mapStateToProps = (state, props) => ({
  user: state.users.find(user => user.uid === props.userId) || {}
});
export default connect(mapStateToProps)(WithUser);
