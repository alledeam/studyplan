import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import PersonIcon from "@material-ui/icons/Person";
import RemoveIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import { blue } from "@material-ui/core/colors";
import UserSearch from "./UserSearch";
import {
  getStudyPlanUsers,
  deleteStudyPlanUsers
} from "../redux/firebase";
import { addUsers } from "../redux/actions";
import Fab from "@material-ui/core/Fab";

const useStyles = () => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  },
  root: {
    minHeight: "300px",
    minWidth: "250px"
  },
  fab: {
    margin: "10px"
  }
});

class SimpleDialog extends React.Component {
  constructor() {
    super();
    this.state = {
      users: [],
      multi: [],
      studyPlanUsers: [],
      selectedUsers: [],
      unSelectedUsers: []
    };
  }
  listenes = [];
  componentDidMount() {
    const plan = this.props.plan;
    const unSubscribeStudyPlanFn = getStudyPlanUsers(
      plan.docId,
      studyPlanUsers => {
        const selectedUsers = this.props.users.filter(u =>
          studyPlanUsers.find(p => p.docId === u.docId)
        ).filter(u=>!u.isDeleted);
        this.setState({ studyPlanUsers, selectedUsers });
      }
    );
    this.listenes.push(unSubscribeStudyPlanFn);
  }
  componentWillUnmount() {
    this.listenes.map(unSubscribeFn => unSubscribeFn());
    this.listenes=[];
  }

  handleAddButton = () => {
    const { multi } = this.state;
    this.props.addUsers({ users: multi, plan: this.props.plan }).then(data => {
      this.setState({multi : [] })
      this.props.handleClose();
    });
  };
  getSelectedUSers = value => {
    const { users, selectedUsers } = this.state;
    if (value && value.length) {
      const filtererdUsers = selectedUsers.filter(u =>
        value.find(m => m.value !== u.docId)
      );
      this.setState(() => ({
        multi: value,
        filtererdUsers: filtererdUsers
      }));
    } else {
      this.setState(() => ({
        multi: [],
        filtererdUsers: users
      }));
    }
  };

  deleteUser = user => {
    const { plan } = this.props;
    const unSubscribeStudyPlanFn = deleteStudyPlanUsers(
      { plan, user },
      studyPlanUsers => {
        const selectedUsers = this.state.users.filter(u =>
          studyPlanUsers.find(p => p.docId === u.docId)
        );
        this.setState({ studyPlanUsers, selectedUsers });
      }
    );
    this.listenes.studyPlanUsers = unSubscribeStudyPlanFn;
  };

  render() {
    const { handleClose, open, isAdmin, classes } = this.props;
    const { selectedUsers } = this.state;
    return (
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
        maxWidth={"md"}
      >
        <div className="dialoge-width">
        <DialogTitle id="simple-dialog-title">Users</DialogTitle>
        <List className={classes.root}>
          {isAdmin && (
            <ListItem>
              <UserSearch
                getSelectedUSers={this.getSelectedUSers}
                multi={this.state.multi}
                users={this.props.users}
                studyPlanUsers={this.state.studyPlanUsers}
              />
              <ListItemAvatar>
                <Fab
                  color="primary"
                  aria-label="Add"
                  className={classes.fab}
                  onClick={this.handleAddButton}
                  size="small"
                >
                  <AddIcon />
                </Fab>
              </ListItemAvatar>
            </ListItem>
          )}
          {selectedUsers &&
            selectedUsers.map(user => (
              <ListItem key={user.email}>
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <PersonIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={
                  <div>
                    {user.username}
                    <br/>
                    <span style={{ color : "grey", fontSize: "14px"}}>{user.email}</span>
                  </div>
                } />
                {isAdmin && (
                  <ListItemAvatar>
                    <Fab
                      color="secondary"
                      aria-label="Add"
                      className={classes.fab}
                      onClick={() => this.deleteUser(user)}
                      size="small"
                    >
                      <RemoveIcon />
                    </Fab>
                  </ListItemAvatar>
                )}
              </ListItem>
            ))}
        </List>
        </div>

      </Dialog>
    );
  }
}
const mapStateToProps = state => ({
  users: state.users || [],
  isAdmin: state.isAdmin
});
export default connect(
  mapStateToProps,
  { addUsers }
)(withStyles(useStyles)(SimpleDialog));
