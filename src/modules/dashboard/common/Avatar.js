import React from "react";
import Avatar from "@material-ui/core/Avatar";

import WithUser from "./WithUser";
import { getUniqueColorFromID } from "../redux/util";

function CustomAvatar({ userId }) {
  return(
    <Avatar
      aria-label="User"
      style={{ backgroundColor: getUniqueColorFromID(userId) }}
    >
      <WithUser
        userId={userId}
        render={user => <span>{user.username[0].toUpperCase()}</span>}
      />
    </Avatar>
  );
}
export default CustomAvatar;
