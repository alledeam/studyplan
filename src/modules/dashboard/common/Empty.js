import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: "100%",
    textAlign: "center",
    padding : "100px 0px"
  }
});

export default function Empty({ text , hide, color}) {
  const classes = useStyles();
  if(hide){
      return "";
  }
  return (
    <div className={classes.root}>
      <Typography variant="h3" gutterBottom style={{ color : color || "white"}}>
        { text || "No data found"}
      </Typography>
    </div>
  );
}
