import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import { urlify } from "../redux/util";

const useStyles = makeStyles({
  inline: {
    display: "inline",
    wordBreak: "break-word"
  }
});

export default function Text({ text, className , component}) {
  const classes = useStyles();
  const lines = text.split("\n");
  const finalText = lines.map(line => urlify(line)).join("<br/>");
  return (
    <Typography
      component= {component || "p"}
      variant="subtitle1"
      className={classes.inline + ` ${className}`}
      color="textPrimary"
      dangerouslySetInnerHTML={{ __html: finalText }}
    />
  );
}
