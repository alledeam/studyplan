import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import GroupIcon from '@material-ui/icons/Group';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Badge from '@material-ui/core/Badge';
import LocalActivityIcon from '@material-ui/icons/LocalActivity';
import UserInPlan from './UsersInPlan';
import Avatar from './Avatar';
import WithUser from './WithUser';
import Text from './Text';
import Tooltip from '@material-ui/core/Tooltip';
import UserStats from '../pages/users/components/UserStats';

const useStyles = theme => ({
  card: {
    width: '100%',
    borderRadius: '5px 5px 0px 0px'
  },
  bottomActions: {
    justifyContent: 'flex-end',
    padding: '0px',
    paddingRight: '8px'
  }
});

class Plan extends React.Component {
  constructor() {
    super();
    this.state = {
      anchorEl: null,
      open: 0
    };
  }
  handleClickOpen = modalID => {
    this.setState({ open: modalID });
  };

  handleClose = () => {
    this.setState({ open: 0 });
  };
  setMenu = event => {
    this.setState({ anchorEl: event ? event.currentTarget : null });
  };
  render() {
    const { classes, plan, isAdmin, completedActivities } = this.props;
    const { open, anchorEl } = this.state;

    const description = plan.description || 'No Description';
    return (
      <>
        <Card
          className={classes.card}
          style={{ borderRadius: '5px 5px 0px 0px' }}
        >
          <CardHeader
            avatar={<Avatar userId={plan.ownerId} />}
            action={
              isAdmin && (
                <Tooltip title='actions'>
                  <IconButton aria-label='Settings' onClick={this.setMenu}>
                    <MoreVertIcon />
                  </IconButton>
                </Tooltip>
              )
            }
            title={plan.title}
            subheader={
              <WithUser
                userId={plan.ownerId}
                render={user => <span> By {user.username}</span>}
              />
            }
          />

          <CardContent
            style={{
              height: '100%',
              wordBreak: 'break-all',
              overflow: 'hidden',
              paddingBottom: '0px'
            }}
          >
            <div style={{ overflow: 'hidden' }}>
              <Text text={description} />
            </div>
            <UserStats completedActs={completedActivities} plan={plan} />
          </CardContent>
          <CardActions className={classes.bottomActions} disableSpacing>
            <Tooltip title='users'>
              <IconButton
                aria-label='users'
                onClick={() => this.handleClickOpen(1)}
              >
                <Badge badgeContent={plan.totalUsers} color='primary'>
                  <GroupIcon />
                </Badge>
              </IconButton>
            </Tooltip>
            <Tooltip title='current needs'>
              <IconButton aria-label='comments'>
                <Badge
                  badgeContent={plan.totalBacklogActivities}
                  color='primary'
                >
                  <LocalActivityIcon />
                </Badge>
              </IconButton>
            </Tooltip>
          </CardActions>
        </Card>
        <UserInPlan
          open={open === 1}
          handleClose={this.handleClose}
          plan={plan}
        />
        <Menu
          id='simple-menu'
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={() => this.setMenu()}
        >
          <MenuItem
            onClick={() => {
              this.setMenu();
              this.props.handleSelectedPlan();
            }}
          >
            Edit
          </MenuItem>
          <MenuItem onClick={this.props.deletePlan}>Delete</MenuItem>
        </Menu>
      </>
    );
  }
}

export default withStyles(useStyles)(Plan);
