import {
  SHOW_NOTICATION,
  SET_USER,
  SET_USER_DETAILS,
  LOG_OUT,
  SET_ADMIN,
  SET_ALL_USERS,
  SET_DRAGGABLE
} from "./constants";

const initialState = {
  isLoggedIn: false,
  role : 'STUDENT',
  user: {},
  userDetails: {},
  notification: null,
  isAdmin : false,
  users:[],
  draggable: true
};

export default function(state = initialState, action) {
  function setState(payload) {
    return {
      ...state,
      ...payload
    };
  }
  switch (action.type) {
    case SHOW_NOTICATION: {
      return setState({
        notification: {
          ...action.payload,
          id: Date.now()
        }
      });
    }
    case SET_USER: {
      return setState({ isLoggedIn: true, user: action.payload });
    }
    case SET_USER_DETAILS: {
      return setState({ userDetails: action.payload, role : action.payload.role });
    }
    case LOG_OUT: {
      return setState({
        isLoggedIn: false,
        user: {},
        userDetails: {},
        isAdmin: false
      });
    }
    case SET_ADMIN:{
      return setState({ isAdmin : true })
    }
    case SET_ALL_USERS:{
      return setState({ users : action.payload})
    }
    case SET_DRAGGABLE:{
      return setState({ draggable : action.payload })
    }
    default:
      return state;
  }
}
