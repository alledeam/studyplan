import { SHOW_NOTICATION, SET_USER, SET_USER_DETAILS, LOG_OUT, SET_ADMIN, SET_ALL_USERS, SET_DRAGGABLE } from "./constants";


export const showNotication = payload => ({
  type: SHOW_NOTICATION,
  payload
});

export const setUser = payload => ({
  type: SET_USER,
  payload
});

export const setUserDetails = payload => ({
  type: SET_USER_DETAILS,
  payload
});

export const setLogout = () => ({
  type: LOG_OUT
})

export const setAdmin = () =>({
  type: SET_ADMIN
})

export const setAllUsers = (payload) =>({
  type: SET_ALL_USERS,
  payload
})

export const setDraggable = (payload) =>({
  type: SET_DRAGGABLE,
  payload
})