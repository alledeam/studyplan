export const SHOW_NOTICATION = "SHOW_NOTICATION";

export const SET_USER = "SET_USER";

export const SET_USER_DETAILS = "SET_USER_DETAILS";

export const LOG_OUT = "LOG_OUT";

export const SET_ADMIN = "SET_ADMIN";

export const SET_ALL_USERS = "SET_ALL_USERS"

export const SET_DRAGGABLE = "SET_DRAGGABLE";