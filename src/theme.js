import { createMuiTheme } from "@material-ui/core/styles";

export default createMuiTheme({
    palette: {
      primary: {
        light: "#ae0f0fb5",
        main: "#ae0f0f",
        dark: "#730000",
        contrastText: "#fff"
      },
      secondary:{
        light: "rgba(144, 147, 144, 0.72)",
        main: "#909390",
        dark: "#6d6f6d",
        contrastText: "#fff"
      }
    }
  });
