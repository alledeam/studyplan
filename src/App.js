import React from "react";
import history from "./history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import { Auth, Dashboard } from "./modules";

import { isLoggedIn } from "./services/common";

function App() {
  return (
    <Router history={history}>
      <Switch>
        <PrivateRoute path="/dashboard" component={Dashboard} />
        <AuthRoutes path="/auth" component={Auth} />
        <Route render={() => <Redirect to="/auth" />} />
      </Switch>
    </Router>
  );
}

const AuthRoutes = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isLoggedIn() === false ? (
        <Component {...props} />
      ) : (
        <Redirect to="/dashboard" />
      )
    }
  />
);

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isLoggedIn() === true ? (
        <Component {...props} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

export default App;
