import React from "react";
import { render } from "react-dom";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";

import store from "./redux/store";
import theme from "./theme";
import {
  setUser,
  setUserDetails,
  setLogout,
  setAdmin,
  setAllUsers,
  showNotication
} from "./redux/actions";
import {
  initFirebase,
  onAuthStateChanged,
  fetchUserDetails,
  fetchAllUsers,
  isOnline
} from "./services/common";
import Notification from "./modules/shared/Notification";
import Loader from "./modules/shared/Loader";
import NetWorkStatus from "./modules/shared/NetworkStatus";

import App from "./App";

import "./index.css";

class Start extends React.Component {
  constructor() {
    super();
    this.state = {
      ready: false,
      online: true
    };
  }
  listerners = [];
  fetchUserDetails = () => {
    fetchUserDetails().then(details => {
      store.dispatch(setUserDetails(details));
      if(details.role==='ADMIN'){
        store.dispatch(setAdmin());
      }
      this.setState({ ready: true });
    });

  };

  fetchUsers = () => {
    const unSubscribeFn = fetchAllUsers(users => {
      store.dispatch(setAllUsers(users));
    });
    this.listerners.push(unSubscribeFn);
  };
  unSubscribeListerers = () => {
    this.listerners.map(fn => fn ? fn() : '');
    this.listerners = [];
  };
  componentWillMount() {
    initFirebase();
    onAuthStateChanged(user => {
      if (user) {
        store.dispatch(setUser(user));
        this.fetchUserDetails();
        this.fetchUsers();
      } else {
        this.unSubscribeListerers();
        store.dispatch(setLogout());
        this.setState({ ready: true });
      }
    });
  }
  componentDidMount() {
    this.listerners.push(
        isOnline(online => {
          this.setState({ online });
        })
    );
    document.getElementsByTagName('body')[0]
    .addEventListener('notification', (e)=>{
      const {
        message,
        type
      } = (e.detail || {});
      store.dispatch(
        showNotication({
          type,
          message
        })
      )
    })
  }

  render() {
    const { ready, online } = this.state;
    return (
      <>
        <div style={{ paddingBottom : online ? "0px" : "64px"}}>
          {ready ? <App /> : <Loader />}
        </div>
        {!online && <NetWorkStatus online={online} onClose={()=>{this.setState({online : true})}} />}
      </>
    );
  }
}

render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Notification/>
      <Start />
    </Provider>
  </MuiThemeProvider>,
  document.getElementById("root")
);

serviceWorker.register();
