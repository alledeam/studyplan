import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';

import firebaseConfig from '../config/firebase.config';

export const initFirebase = () => {
  const app = firebase.initializeApp(firebaseConfig);
  firebase.firestore().enablePersistence({ synchronizeTabs: true });
  return app;
};

export const onAuthStateChanged = cb => {
  firebase.auth().onAuthStateChanged(cb);
};

export const isLoggedIn = () => {
  return Boolean(getUser());
};

export const getUser = () => {
  const user = firebase.auth().currentUser;
  return user;
};

export const getUserId = () => {
  const user = getUser() || {};
  return user.uid;
};

export const fetchUserDetails = () => {
  const firestore = firebase.firestore();
  const uid = getUserId();
  return firestore
    .collection('users')
    .doc(uid)
    .get()
    .then(doc => doc.data());
};

export const fetchUserRole = () => {
  const firestore = firebase.firestore();
  const uid = getUserId();
  return firestore
    .collection('users-role')
    .doc(uid)
    .get()
    .then(doc => doc.data());
};

export function snapshotsToData(snaps) {
  const data = [];
  snaps.forEach(doc => {
    data.push({ ...doc.data(), uid: doc.id, docId: doc.id });
  });
  return data;
}
export const fetchAllUsers = cb => {
  const firestore = firebase.firestore();
  return firestore.collection('users').onSnapshot(snaps => {
    return cb(snapshotsToData(snaps));
  });
};

export const isOnline = cb => {
  const connectedRef = firebase.database().ref('.info/connected');
  let timeout = null;
  connectedRef.on('value', function(snap) {
    let val = snap.val();
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(() => {
      if (val === true) {
        cb(true);
      } else {
        cb(false);
      }
    }, 5000);
  });
};

export function deepCopy(obj) {
  let newObj = Array.isArray(obj) ? [] : {};
  for (let o in obj) {
    if (typeof o === 'object' && o != null) {
      newObj[o] = deepCopy(obj[o]);
    } else {
      newObj[o] = obj[o];
    }
  }
  return newObj;
}
