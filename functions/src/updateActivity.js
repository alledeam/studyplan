const admin = require("firebase-admin");

const afs = admin.firestore();

function activityTransaction(planId, activityId, key, change) {
  const sfDocRef = afs
    .collection("study-plans")
    .doc(planId)
    .collection("activities")
    .doc(activityId);
  return afs
    .runTransaction(function(transaction) {
      return transaction.get(sfDocRef).then(function(sfDoc) {
        if (!sfDoc.exists) {
          throw new Error(`${planId} : Document does not exist!`);
        }
        const newValue = (sfDoc.data()[key] || 0) + change;
        transaction.update(sfDocRef, { [key]: newValue });
      });
    })
    .then(function() {
      console.log(`${key}: Transaction successfully committed!`);
    })
    .catch(function(error) {
      console.log(`T${key}: ransaction failed: ", error`);
    });
}

exports.updateActivityCommentsCountFn = function(change, context) {
  const { planId, activityId } = context.params;

  if (change.after.exists && !change.before.exists) {
    //increment
    return activityTransaction(planId, activityId, "totalComments", 1);
  }

  if (!change.after.exists && change.before.exists) {
    //descement
    return activityTransaction(planId, activityId, "totalComments", -1);
  }
  return null;
};
