const admin = require('firebase-admin');
const afs = admin.firestore();
// const afs = admin.firestore();
exports.updateStats = function(activity, planId, isDeleted = false) {
  if (!activity.startTime || !activity.finishTime) {
    return Promise.resolve();
  }
  if (!activity.isCompleted && !isDeleted) {
    return Promise.resolve();
  }
  const activityData = {
    startTime: activity.startTime,
    finishTime: activity.finishTime,
    activityID: activity.activityID,
    planId: planId
  };
  function getPlanUsers(planId) {
    return afs
      .collection('study-plans')
      .doc(planId)
      .collection('users')
      .get()
      .then(docs => {
        const studyPlansUsers = [];
        docs.forEach(doc => {
          studyPlansUsers.push(doc.id);
        });
        return studyPlansUsers;
      });
  }

  return getPlanUsers(planId).then(sPusers => {
    const users = sPusers;
    if (activity.completedBy) {
      users.push(activity.completedBy);
    }
    return Promise.all(
      users.map(userId =>
        afs
          .collection('users')
          .doc(userId)
          .get()
      )
    ).then(data => {
      return data
        .map(doc => ({ role: doc.data().role || 'STUDENT', id: doc.id }))
        .filter(
          u =>
            u.role === 'STUDENT' ||
            (u.id === activity.completedBy && u.role !== 'STUDENT')
        )
        .map(user => {
          const keyToUpdate =
            user.id === activity.completedBy ? 'registered' : 'usages';

          if (user.id === activity.completedBy) {
            isDeleted = activity.excludedTeacher;
          } else {
            isDeleted = activity.excludedStudent;
          }
          console.log('user===', user, isDeleted);
          return updateUserStats(user, activityData, isDeleted, keyToUpdate);
        });
    });
  });
};

function updateUserStats(user, activity, isDeleted, keyToUpdate) {
  const sfDocRef = afs.collection('users-tickets').doc(user.id);
  return afs.runTransaction(function(transaction) {
    return transaction.get(sfDocRef).then(function(sfDoc) {
      const emptyValue = {
        registered: [],
        usages: [],
        tickets: [],
        role: user.role
      };
      const newValue = sfDoc.exists
        ? {
            ...emptyValue,
            ...sfDoc.data()
          }
        : {
            ...emptyValue
          };
      const data = newValue[keyToUpdate];
      let updatedData = data;
      if (isDeleted) {
        return sfDoc.exists
          ? transaction.update(sfDocRef, {
              [keyToUpdate]: data.filter(
                a => a.activityID !== activity.activityID
              )
            })
          : transaction.set(sfDocRef, {
              ...newValue
            });
      }
      if (data.length === 0) {
        updatedData = [{ ...activity }];
      } else {
        const exist = data.find(n => n.activityID === activity.activityID);
        if (!exist) {
          updatedData = [{ ...activity }, ...data];
        } else {
          const updated = data.map(r => {
            if (r.activityID === activity.activityID) {
              r.startTime = activity.startTime;
              r.finishTime = activity.finishTime;
            }
            return r;
          });
          updatedData = updated;
        }
      }
      console.log('updated data', keyToUpdate, updatedData);
      return sfDoc.exists
        ? transaction.update(sfDocRef, {
            [keyToUpdate]: updatedData
          })
        : transaction.set(sfDocRef, {
            ...newValue,
            [keyToUpdate]: updatedData
          });
    });
  });
}
