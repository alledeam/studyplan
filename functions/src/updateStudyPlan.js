const admin = require('firebase-admin');
const afs = admin.firestore();
const { updateStats } = require('./update-user-stats');

function updateUserStudyPlan(planId, userId, isAdd) {
  const sfDocRef = afs.collection('users-study-plans').doc(userId);
  return afs
    .runTransaction(function(transaction) {
      // This code may get re-run multiple times if there are conflicts.
      return transaction.get(sfDocRef).then(function(sfDoc) {
        const sfData = sfDoc.data() || {};
        if (isAdd) {
          sfData[planId] = true;
        } else {
          delete sfData[planId];
        }
        transaction.set(sfDocRef, sfData);
      });
    })
    .then(function() {
      console.log('Transaction successfully committed!');
    })
    .catch(function(error) {
      console.log('Transaction failed: ', error);
    });
}

function studyPlanTransaction(planId, key, change) {
  const sfDocRef = afs.collection('study-plans').doc(planId);
  return afs
    .runTransaction(function(transaction) {
      return transaction.get(sfDocRef).then(function(sfDoc) {
        if (!sfDoc.exists) {
          throw new Error(`${planId} : Document does not exist!`);
        }
        const newValue = (sfDoc.data()[key] || 0) + change;
        transaction.update(sfDocRef, { [key]: newValue });
      });
    })
    .then(function() {
      console.log(`${key}: Transaction successfully committed!`);
    })
    .catch(function(error) {
      console.log(`T${key}: ransaction failed: ", error`);
    });
}

exports.updateUsersCountFn = function(change, context) {
  const { planId, userId } = context.params;

  if (change.after.exists && !change.before.exists) {
    //increment
    return Promise.all([
      updateUserStudyPlan(planId, userId, true),
      studyPlanTransaction(planId, 'totalUsers', 1)
    ]).then(() => true);
  }

  if (!change.after.exists && change.before.exists) {
    //decrease count
    return Promise.all([
      updateUserStudyPlan(planId, userId, false),
      studyPlanTransaction(planId, 'totalUsers', -1)
    ]).then(() => true);
  }
  return null;
};

exports.updateActivitiesCountFn = function(change, context) {
  const { planId, activityId } = context.params;

  if (change.after.exists && !change.before.exists) {
    console.log('new doc inserted--------');
    const activity = change.after.data();
    const key = activity.isBacklog
      ? 'totalBacklogActivities'
      : 'totalActivities';
    //increment
    return studyPlanTransaction(planId, key, 1);
    // return Promise.all([
    //   studyPlanTransaction(planId, key, 1),
    //   updateStats(
    //     {
    //       ...activity,
    //       activityID : activityId
    //     },
    //     planId,
    //     false
    //   )
    // ])
    // .then(()=>true);
  }

  if (!change.after.exists && change.before.exists) {
    console.log('doc deleted');
    const activity = change.before.data();
    const key = activity.isBacklog
      ? 'totalBacklogActivities'
      : 'totalActivities';
    //decrement
    return Promise.all([
      studyPlanTransaction(planId, key, -1),
      updateStats(
        {
          ...activity,
          activityID: activityId
        },
        planId,
        true
      )
    ]).then(() => true);
  }
  if (change.after.exists && change.before.exists) {
    const activity = change.after.data();
    const oldActivity = change.before.data();
    console.log('--doc updated--', activity.isCompleted);
    return updateStats(
      {
        ...activity,
        activityID: activityId
      },
      planId,
      activity.isCompleted
    );
    // if (activity.isCompleted) {
    //   console.log('activity updated');
    //   // if(!activity.startTime.isEqual(oldActivity.startTime) || !activity.finishTime.isEqual(oldActivity.finishTime)){
    //   return updateStats(
    //     {
    //       ...activity,
    //       activityID: activityId
    //     },
    //     planId,
    //     false
    //   );
    //   // }
    // } else {
    //   if (!oldActivity.isCompleted) {
    //     return updateStats(
    //       {
    //         ...activity,
    //         activityID: activityId
    //       },
    //       planId,
    //       true
    //     );
    //   }
    // }
  }
  return Promise.resolve();
};

exports.studyPlanByIDTransaction = studyPlanTransaction;