const admin = require("firebase-admin");

const afs = admin.firestore();

let studyPlansUsers = [];
let users = [];
function getPlanUsers(planId) {
  return afs
    .collection("study-plans")
    .doc(planId)
    .collection("users")
    .get()
    .then(docs => {
      docs.forEach(doc => {
        studyPlansUsers.push(doc.id);
      });
      return studyPlansUsers;
    });
}

exports.updateUserTicket = (planId, activity, activityID) => {
  getPlanUsers(planId).then(sPusers => {
    const userPromise = sPusers.map(user => {
      return afs
        .collection("users")
        .doc(user)
        .get()
        .then(doc => {
          users = [{ data: doc.data(), id: doc.id }, ...users];
          return users;
        });
    });
    let pr = Promise.all(userPromise);
    pr.then(data => {
      return users.map(user => {
        const sfDocRef = afs.collection("users-tickets").doc(user.id);
        return afs
          .runTransaction(function(transaction) {
            return transaction.get(sfDocRef).then(function(sfDoc) {
              if (!sfDoc.exists) {
                throw new Error(`${planId} : Document does not exist!`);
              }
              const newValue = sfDoc.data();
            
              if (user.data.role === "TEACHER") {
                let registered;
                if (!newValue.registered.length) {
                  registered = [{ ...activity, activityID }];
                } else {
                  let exist = newValue.registered.find(
                    n => n.activityID === activityID
                  );
                  if (!exist) {
                    registered = [
                      { ...activity, activityID },
                      ...newValue.registered
                    ];
                  } else {
             
                    let updated = newValue.registered.map(r => {
                      if (r.activityID === activityID) {
                        r.startTime = activity.startTime;
                        r.finishTime = activity.finishTime;
                      }
                      return r;
                    });
                    registered = updated;
                  }
                }
                transaction.update(sfDocRef, {
                  registered
                });
              } else if (user.data.role === "STUDENT") {
               
                let usages;
                if (!newValue.usages.length) {
                  usages = [{ ...activity, activityID }];
                } else {
                  let exist = newValue.usages.find(
                    n => n.activityID === activityID
                  );
                  if (!exist) {
                    usages = [{ ...activity, activityID }, ...newValue.usages];
                  } else {
                   
                    let updated = newValue.usages.map(r => {
                      if (r.activityID === activityID) {
                        r.startTime = activity.startTime;
                        r.finishTime = activity.finishTime;
                      }
                      return r;
                    });
                    usages = updated;
                  }
                }
                transaction.update(sfDocRef, {
                  usages
                });
              }
            });
          })
          .then(function() {
            console.log(` Transaction successfully committed!`);
          })
          .catch(function(error) {
            console.log(` ransaction failed: ", ${error}`);
          });

        return user;
      });
    });
  });
};

exports.createUserTicket=(userId, user)=>{
  return afs.collection('users-tickets').doc(userId).set({
    role : user.role,
    usages : [],
    tickets : [],
    registered: []
  })
}