const admin = require('firebase-admin');
const afs = admin.firestore();

const { studyPlanByIDTransaction } = require('./updateStudyPlan');

exports.deleteUser = userId => {
  return Promise.all([
    admin
      .auth()
      .deleteUser(userId)
      .then(function() {
        console.log('Successfully deleted user', userId);
      })
      .catch(function(error) {
        console.log('Error deleting user:', error);
      }),
    afs
      .collection('users')
      .doc(userId)
      .update({
        isDeleted: true
      }),
    removeUserFromStudyPlans(userId)
  ]);
};

function removeUserFromStudyPlans(userId) {
  return afs
    .collection('users-study-plans')
    .doc(userId)
    .get()
    .then(doc => {
      console.log('deleted users study plans', doc.data());
      return Object.keys(doc.data() || {}).map(planId =>
        studyPlanByIDTransaction(planId, 'totalUsers', -1)
      );
    });
}
