const admin = require('firebase-admin');
const nodemailer = require('nodemailer');
const getTemplate = require('./templates/email');

const afs = admin.firestore();

function getTransporter() {
  return nodemailer.createTransport({
    host: 'smtp.mailtrap.io',
    port: 2525,
    auth: {
      user: 'xxxxxxx', //set your email
      pass: 'xxxxxx' //set your password
    }
  });
}

function deletePassword(userId) {
  return afs
    .collection('users')
    .doc(userId)
    .update({
      password: admin.firestore.FieldValue.delete()
    });
}

exports.sendMail = function(userId, user) {
  console.log(user);
  // send mail with defined transport object
  const transporter = getTransporter();
  return Promise.all([
    (transporter.sendMail({
      from: 'admin@alledeam.com', // sender address
      to: user.email, // list of receivers
      subject: 'Invitation from Alledeam', // Subject line
      html: getTemplate(user) // html body
    }),
    deletePassword(userId))
  ]);
};
