const admin = require('firebase-admin');

var serviceAccount = require('../service-account.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://studyplans-98fd3.firebaseio.com'
});

function createUser(){
    admin
    .auth()
    .createUser({
      uid: '',
      email: ''
    })
    .then(function(userRecord) {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log('Successfully created new user:', userRecord.uid);
    })
    .catch(function(error) {
      console.log('Error creating new user:', error);
    });
}

