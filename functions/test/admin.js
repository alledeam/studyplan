const admin = require("firebase-admin");
const fs = require("fs");
const { updateStats } = require("../src/update-user-stats");
var serviceAccount = require("../service-account.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://studyplans-98fd3.firebaseio.com"
});

const afs = admin.firestore();
// const activity = {
//   startTime: new Date(),
//   finishTime: new Date(),
//   activityID: "k92k6TfRZK5fhVc3T3o2"
// };
// const planId = "gm85knWrZOCHWJ3rznfS";

// async function update(activity, planId, isDeleted){
//   let users = [];
//   let studyPlansUsers = [];
//   function getPlanUsers(planId) {
//     return afs
//       .collection("study-plans")
//       .doc(planId)
//       .collection("users")
//       .get()
//       .then(docs => {
//         docs.forEach(doc => {
//           // console.log(doc.id, doc.data());
//           studyPlansUsers.push(doc.id);
//         });
//         return studyPlansUsers;
//       });
//   }

//   return await getPlanUsers(planId).then(sPusers => {
//     // console.log("sPusers", sPusers);
//     const userPromise = sPusers.map(user => {
//       return afs
//         .collection("users")
//         .doc(user)
//         .get()
//         .then(doc => {
//           users = [{ data: doc.data(), id: doc.id }, ...users];
//           return users;
//         });
//     });
//     return Promise.all(userPromise)
//     .then(data => {
//       // let students = users
//       //   .filter(u => u.data.role !== "TEACHER")
//       //   .map(user => {
//       //     return user.id;
//       //   });
//       // let teachers = users
//       //   .filter(u => u.data.role !== "STUDENT")
//       //   .map(user => {
//       //     return user.id;
//       //   });
//       return users.map(user => {
//         const sfDocRef = afs.collection("users-tickets").doc(user.id);
//         return afs.runTransaction(function(transaction) {
//           return transaction.get(sfDocRef).then(function(sfDoc) {
//             // if (!sfDoc.exists) {
//             //   throw new Error(`${planId} : Document does not exist!`);
//             // }
//             const emptyValue = {
//               registered : [],
//               usages:[],
//               tickets:[],
//               role: user.data.role
//             }
//             const newValue = sfDoc.exists ?
//             {
//               ...emptyValue,
//               ...sfDoc.data(),
//             } : {
//               ...emptyValue
//             };
//             // console.log(
//             //   "******************uuuuuuuuuuu***********",
//             //   sfDoc.data(),
//             //   sfDoc.id
//             // );
//             if (user.data.role === "TEACHER" && activity.completedBy === user.id) {
//               if(isDeleted){
//                 return sfDoc.exists ? transaction.update(sfDocRef, {
//                   registered : newValue.registered.filter(a=>a.activityID !== activity.activityID)
//                 }) : transaction.set(sfDocRef, {
//                   ...newValue,
//                 });
//               }
//               let registered;
//               if (newValue.registered.length===0) {
//                 registered = [{ ...activity }];
//               } else {
//                 let exist = newValue.registered.find(
//                   n => n.activityID === activity.activityID
//                 );
//                 if (!exist) {
//                   registered = [{ ...activity }, ...newValue.registered];
//                 } else {
//                   let updated = newValue.registered.map(r => {
//                     if (r.activityID === activity.activityID) {
//                       r.startTime = activity.startTime;
//                       r.finishTime = activity.finishTime;
//                     }
//                     return r;
//                   });
//                   registered = updated;
//                 }
//               }
//               console.log("done");
//               return sfDoc.exists ? transaction.update(sfDocRef, {
//                 registered
//               }) : transaction.set(sfDocRef, {
//                 ...newValue,
//                 registered
//               });
//             } else if (user.data.role === "STUDENT") {
//               if(isDeleted){
//                 return sfDoc.exists ? transaction.update(sfDocRef, {
//                   usages : newValue.usages.filter(a=>a.activityID !== activity.activityID)
//                 }) : transaction.set(sfDocRef, {
//                   ...newValue,
//                 });
//               }
//               let usages;
//               if (newValue.usages.length===0) {
//                 usages = [{ ...activity }];
//               } else {
//                 let exist = newValue.usages.find(
//                   n => n.activityID === activity.activityID
//                 );

//                 if (!exist) {
//                   usages = [{ ...activity }, ...newValue.usages];
//                 } else {
//                   let updated = newValue.usages.map(r => {
//                     if (r.activityID === activity.activityID) {
//                       r.startTime = activity.startTime;
//                       r.finishTime = activity.finishTime;
//                     }
//                     return r;
//                   });
//                   usages = updated;
//                 }
//               }
//               console.log("done");
//               return sfDoc.exists ? transaction.update(sfDocRef, {
//                 usages
//               }) : transaction.set(sfDocRef, {
//                 ...newValue,
//                 usages
//               });
//             }
//           });
//         });
//       });
//     });
//   });
// }


//************************FETCH ACTIVOITIES AND SAVE ************************* */
// let activities = [];
// function fetchPlans(){
//   return afs.collection('study-plans').get()
//   .then(async (planSnaps)=>{
//     console.log('plans fetched');
//     const plans = [];
//     planSnaps.forEach((planSnap)=>{
//       const planId = planSnap.id;
//       plans.push(planId);
//     })
//     for await(let planId of plans){
//       console.log('ok');
//       await fetchActivities(planId);
//     }
//     fs.writeFileSync("./activities.json", JSON.stringify(
//         activities, null, 2
//     ))
//   })
// }

// async function fetchActivities(planId){
//    return afs.collection(`study-plans/${planId}/activities`)
//   .where("isCompleted", "==", true)
//   .get()
//   .then((actSnaps)=>{
//     actSnaps.forEach((actSnap)=>{
//     console.log('activity fetched');
//     const data = actSnap.data();
//       const activity = {
//         ...data,
//         activityID: actSnap.id,
//         startTime : data.startTime ? data.startTime.toDate() : null,
//         finishTime: data.finishTime ? data.finishTime.toDate() : null
//       }
//       activities.push({
//         planId : planId,
//         activity
//       })
//     });
//   })
// }
// fetchPlans();
//************************FETCH ACTIVOITIES AND SAVE ************************* */



//************************ load And Update User Tickets ************************* */

const fileText = fs.readFileSync("./activities.json");
const activities = JSON.parse(fileText);
setTimeout(()=>{
  loadAndUpdateActivity();
},3000)

function delay(sec=1){
  return new Promise(res=>{
    setTimeout(()=>{
      res();
    },sec*1000)
  })
}

async function loadAndUpdateActivity(){
  console.log(activities.length);
  for await(let data of activities.reverse()){
    const act = {
      ...data,
      startTime: new Date(data.activity.startTime),
      finishTime: new Date(data.activity.finishTime),
      activityID: data.activity.activityID,
      planId: data.planId,
    }
    if(data.activity.isCompleted && data.activity.startTime && data.activity.finishTime){
      try{
        await updateStats(act , data.planId, 
          !act.excluded
          , afs);
        await delay(1);
      }
      catch(err){
        console.log(act.activityID);
      }
    }
  }

}

//************************ load And Update User Tickets  ************************* */



//************************ Update user tickets ************************* */

// async function fff(){

//   let activities = [];
//   function trans(d){
//     const p = activities.find(a=>a.activity.activityID === d.activityID);
//     return {
//       startTime: d.startTime ? typeof d.startTime==='string' ? new Date(d.startTime) : d.startTime : d.createdAt,
//       finishTime: d.finishTime ? typeof d.finishTime==='string' ? new Date(d.finishTime) : d.finishTime : d.completedAt,
//       activityID: d.activityID,
//       planId: p ? p.planId : ""
//     }
//   }


//   const fileText = fs.readFileSync("./activities-old.json");
//   activities = JSON.parse(fileText);
//   afs.collection('users-tickets').get()
//     .then(async (snaps)=>{
//       const activityies = [];
//       snaps.forEach((snap)=>{
//         activityies.push(snap);
//       })
//       let index = 0;
//       for await( let snap of activityies){
//         const data = snap.data();
//         const id = snap.id;
//         console.log(id, data);
//         console.log(index++);
//         await afs.collection("users-tickets").doc(id).update({
//           usages : data.usages ? data.usages.map(trans) : [],
//           registered : data.registered ? data.registered.map(trans) : [],
//         })
//       }
//     })
// }
// fff();
//************************Update user tickets ************************* */



// let users = [];
// function fetchPlans(){
//   return afs.collection('study-plans').get()
//   .then(async (planSnaps)=>{
//     console.log('plans fetched');
//     const plans = [];
//     planSnaps.forEach((planSnap)=>{
//       const planId = planSnap.id;
//       plans.push(planId);
//     })
//     for await(let planId of plans){
//       console.log('ok');
//       await fetchUsers(planId);
//     }
//     console.log("RUNNNNNNNNNnn")
//     fs.writeFileSync("./users.json", JSON.stringify(
//         users, null, 2
//     ))
//   })
// }

// async function fetchUsers(planId){
//    return afs.collection(`study-plans/${planId}/users`)
//   .get()
//   .then((actSnaps)=>{
//     actSnaps.forEach((actSnap)=>{
//     console.log('users fetched');
//     // const data = actSnap.data();
//       users.push({
//         userId : actSnap.id,
//         planId
//       })
//     });
//   })
// }
// fetchPlans();

// async function loadUsers(){
//   const fileText = fs.readFileSync("./users.json");
//   const data = JSON.parse(fileText);
//   const users = {};
//   data.forEach(d=>{
//      users[d.userId] =  {
//       ...(users[d.userId] || {}),
//       [d.planId]: true
//     };
//   })
//   for await(let userId of Object.keys(users)){
//           await saveUserStudyPlan(userId, users[userId]);
//         }
//   console.log(users);
// }

// loadUsers();

// function saveUserStudyPlan(userId, plans){
//   return afs.collection("users-study-plans")
//   .doc(userId)
//   .set(plans);
// }
// function userStudeyPlans(){
//   const users  = [];
//   afs.collection("users-study-plans")
//   .get()
//   .then(snaps=>{
//     snaps.forEach(snap=>{
//       users.push({
//         userId: snap.id,
//         data:snap.data()
//       })
//     })
//     fs.writeFileSync("./users-study-plans.json", JSON.stringify(
//               users, null, 2
//       ))
//   })
// }
// userStudeyPlans()
