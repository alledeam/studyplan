const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp(functions.config().firebase);

const { sendMail } = require("./src/sendMail");
const {
  updateUsersCountFn,
  updateActivitiesCountFn
} = require("./src/updateStudyPlan");

const { updateActivityCommentsCountFn } = require("./src/updateActivity");
const {createUserTicket}=require("./src/updateUserTicket");
const { deleteUser } = require('./src/deleteUser');

exports.updateUsersCount = functions.firestore
  .document("study-plans/{planId}/users/{userId}")
  .onWrite((change, context) => {
    return updateUsersCountFn(change, context);
  });

exports.updateActivitiesCount = functions.firestore
  .document("study-plans/{planId}/activities/{activityId}")
  .onWrite((change, context) => {
    return updateActivitiesCountFn(change, context);
  });

exports.updateActivityCommentsCount = functions.firestore
  .document("study-plans/{planId}/activities/{activityId}/comments/{commentId}")
  .onWrite((change, context) => {
    return updateActivityCommentsCountFn(change, context);
  });

exports.sendEmailToUser = functions.firestore
  .document("users/{userId}")
  .onCreate((snap, context) => {
    const {  userId} = context.params;
    const user = snap.data();

    return Promise.all([
      createUserTicket(userId, user),
      sendMail(userId, user)
    ])
  });

  exports.deleteUserAccount = functions.firestore
  .document('deleted-users/{userId}')
  .onCreate((snap, context) => {
    const { userId } = context.params;
    return deleteUser(userId);
});
