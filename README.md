# Alledeam

### Tech Stack

- React
- Firestore
- Firebase functions

### Installation

```sh
$ git clone https://walkwel_dev@bitbucket.org/walkwel_dev/alledeam.git
$ cd alledeam
$ npm install
$ npm start
```

### Firebase login/setup

https://firebase.google.com/docs/hosting/deploying

```sh
 $ firebase login
```

## function setup

https://firebase.google.com/docs/hosting/deploying

```sh
$ firebase login
$ cd functions
$ npm i
$ firebase deploy --only functions
```

### Deploy

```sh
$ npm run build
$ npm run host
```

### Update mail credentials to send invitation mail

##### Open functions/src/sendMail.js

```

function getTransporter() {
  return nodemailer.createTransport({
    host: 'smtp.mailtrap.io',
    port: 2525,
    auth: {
      user: 'xxxxxxx', //set your email
      pass: 'xxxxxx' //set your password
    }
  });
}
```

##### Deploy sendEmail firebase function

```sh
firebase deploy --only functions:sendEmailToUser
```
